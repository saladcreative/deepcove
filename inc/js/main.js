// @codekit-prepend "foundation.min.js";
// @codekit-prepend "modernizr-custom.js";
// @codekit-prepend "parsley.js";
// @codekit-prepend "what-input.js";c

// Animationg hamburger
$(document).ready(function(){
	$('#nav-icon3').click(function(){
		$(this).toggleClass('open');
	});
});

// Mobile menu expand
$(document).ready(function(){
	$( ".mobile-links-menu" ).hide();
	$( ".hamburger" ).click(function() {
		$( ".mobile-links-menu" ).slideToggle( "slow", function() {
			$( "mobile-links-menu" ).show();
		});
	});
});

// Page loading
$(function(){
    $(".menu a").click(function(evt){
        evt.preventDefault();

         $(".loadinganim").fadeIn(150);
         $(".js .page-loader").fadeIn(150);

        var link = $(this).attr("href");
        setTimeout(function() {
            window.location.href = link;
        }, 220);
    });
});
function silkysmoothpageload() {
    setTimeout(function() {
        $(".loadinganim").fadeOut(1000);
    }, 1000);
    setTimeout(function() {
        $(".js .page-loader").fadeOut(200);
        // $(".page-loader").css('height', 'auto');
    }, 1000);
};

// Sticky header
$(window).scroll(function () {
    if( $(window).scrollTop() > $('.white-bg, .white-mobile').offset().top && !($('.white-bg, .white-mobile').hasClass('posi'))){
    	$('.white-bg, .white-mobile').fadeIn(function(){
    		$('.white-bg, .white-mobile').addClass('posi');
    		$('.white-logo').hide();
    		$('.vertical.medium-horizontal.menu li a').addClass('coloured-text');
    		$('.coloured-logo').show();
    	})
    } else if ($(window).scrollTop() == 0){
    	$('.white-bg, .white-mobile').fadeIn(function(){
    		$('.white-bg, .white-mobile').removeClass('posi');
    		$('.coloured-logo').hide();
    		$('.vertical.medium-horizontal.menu li a').removeClass('coloured-text');
    		$('.white-logo').show();
    	})
    }
});

// $(window).load(function(){
//     silkysmoothpageload();
// });

// Detect Smaller device
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {
   $('iframe').hide();
   $('.hidden-text').show();
}

$(document).ready(function(){
    $('.your-class').slick({
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
    });
  });