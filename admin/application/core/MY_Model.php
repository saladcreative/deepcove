<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
    
    function return_all($orderby = false)
	{
		if($orderby) $this->db->order_by($orderby);
		return $this->db->get($this->table);
	}
	
	function return_one_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get($this->table);
	}
	
	function return_one_by_slug($slug)
	{
		$this->db->where('slug', $slug);
		$this->db->limit(1);
		return $this->db->get($this->table);
	}

	
	function delete($id)
	{
		$this->db->where('id', $id);		
		$this->db->delete($this->table);
	}
	
	
	function slugchecker($tempslug)
	{
		//check for duplicate slug
		$query = $this->db->get_where($this->table, array('slug' => $tempslug));
		if($query->num_rows() > 0) {
			$i=1;
			while($query->num_rows() > 0){
				$slug = $tempslug.$i;
				$query = $this->db->get_where($this->table, array('slug' => $slug));
				$i++;
			}
		} else {
			$slug = $tempslug;
		}
		//check for duplicate slug
		
		return $slug;
		
	}
	
function uploadimg($id, $table='', $uniqueid='',$fieldname='')
    {
	    		
		$config['upload_path'] = '../uploads/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['max_size']	= '15120';
		$this->load->library('upload', $config);
		
		if($table=='') {
			$table = $this->table;
		}
		
		if($uniqueid=='') {
			$uniqueid = 'id';
		}
		
		if($fieldname=='') {
			$fieldname = 'img';
		}
		
		if ($this->upload->do_upload($fieldname)) {
			$up = $this->upload->data();
						
			$this->db->update($table, array($fieldname => $up['file_name']), array($uniqueid => $id));
			return 1;
		} else {
			return $this->upload->display_errors('', '');
		}
	    
	    
    }


}