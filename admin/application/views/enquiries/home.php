
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
				
				<div class="wrapper">
				
					<?php if($this->session->flashdata('msg') != '') {	?>	
					<div class="alert alert-info fade in top-23">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?=$this->session->flashdata('msg')?>
					</div>
					<?php } if($this->session->flashdata('msg_err') != '') { ?>
					<div class="alert alert-error fade in top-23">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?=$this->session->flashdata('msg_err')?>
					</div>
					<?php } ?>
					
					
					<div class="whead top-23">
						<strong>Contact Page Enquiries</strong>
						<div class="buttontr"><a href="<?=site_url($this->router->class.'/export')?>" class="btn">Export all</a></div>
					</div>
					
					<div class="table-search-holder sizing">
						<input type="text" class="table-search" placeholder="Search">
					</div>

					<div class="box holder tblr twhite">
						<table width="100%" cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php if ($query->num_rows() > 0) {
								foreach ($query->result() as $row) { ?>
								<tr>
									
									<td><?=$this->typography->format_characters($row->name)?></td>
									<td><?=$this->typography->format_characters($row->email)?></td>
									<td class="action">
										<a href="#delete<?=$row->id?>" class="ablanc tip" href="javascript:;" title="Delete" data-toggle="modal"><i class="icon-remove"></i></a>
										<!-- Delete Modal -->
										<div id="delete<?=$row->id?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												<h3 id="myModalLabel">Delete entry?</h3>
											</div>
											<div class="modal-body">
												<p>Are you sure that you want to delete this entry permanently?</p>
											</div>
											<div class="modal-footer">
												<?=form_open($this->router->class.'/delete')?>
													<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
													<?=form_hidden('id', $row->id)?>
													<?=form_submit('submit', 'Yes - Delete', 'class="btn btn-danger"')?>
												</form
											</div>
										</div>
										<!-- Delete Modal -->
										
									</td>
								</tr>
								<?php } } else { ?>
								<tr>	
									<td colspan="4">No results found</td>
								</tr>
								<?php }  ?>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div> <!-- /container -->
		