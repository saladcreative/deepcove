
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
				
				<div class="wrapper">
					
					<?=form_open_multipart($this->router->class.'/save', 'class="main validateform"')?>
						
						<div class="whead top-23">
							<strong><?=$formtitle?></strong>
						</div>
						
						<div class="box holder type-tip">
							
							<div class="row">
								<div class="grid1">
									<label>Page Title</label>
								</div>
								<div class="grid2">
									<?=form_input('title', $row->title, 'class="shiny required"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>Second heading</label>
								</div>
								<div class="grid2">
									<?=form_input('heading', $row->heading, 'class="shiny required"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>Second Heading Body</label>
								</div>
								<div class="grid2">
									<?=form_textarea('body', $row->body, 'class="redactor shiny required"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>Button title</label>
								</div>
								<div class="grid2">
									<?=form_input('buttontitle', $row->buttontitle, 'class="shiny required"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>Panel one title</label>
								</div>
								<div class="grid2">
									<?=form_input('boxonetitle', $row->boxonetitle, 'class="shiny required"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>Panel one body</label>
								</div>
								<div class="grid2">
									<?=form_textarea('boxonebody', $row->boxonebody, 'class="redactor shiny required"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>Panel two title</label>
								</div>
								<div class="grid2">
									<?=form_input('boxtwotitle', $row->boxtwotitle, 'class="shiny required"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>Panel two body</label>
								</div>
								<div class="grid2">
									<?=form_textarea('boxtwobody', $row->boxtwobody, 'class="redactor shiny required"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>Panel three title</label>
								</div>
								<div class="grid2">
									<?=form_input('boxthreetitle', $row->boxthreetitle, 'class="shiny required"')?>
								</div>
							</div>							
							<div class="row">
								<div class="grid1">
									<label>Panel three body</label>
								</div>
								<div class="grid2">
									<?=form_textarea('boxthreebody', $row->boxthreebody, 'class="redactor shiny required"')?>
								</div>
							</div>


							<div class="row">
								<div class="grid1">
									<label>Slide 1</label>
								</div>
								<div class="grid2">
									<?=form_input('slide1title', $row->slide1title, 'class="shiny"')?>
									<?=form_textarea('slide1text', $row->slide1text, 'class="redactor shiny"')?>
									<div class="">
										<span class="btn btn-inverse fileinput-button">
											<i class="icon-plus icon-white"></i>
											<span>Select file</span>
											<?=form_upload('slide1image', '', 'id="fileupload"')?>
										</span>
										<div class="clear"></div>
										<span id="filepath"></span>
										<?php if($row->slide1image != '') { ?>
										<img src="<?=substr(base_url(), 0, -6)?>uploads/slir/w400-q90/<?=rawurlencode($row->slide1image)?>" alt="Deepcove EU" id="tn" />
										<?php } ?>
									</div>
								</div>
							</div>
							<!-- Slide 2 -->
							<div class="row">
								<div class="grid1">
									<label>Slide 2</label>
								</div>
								<div class="grid2">
									<?=form_input('slide2title', $row->slide2title, 'class="shiny"')?>
									<?=form_textarea('slide2text', $row->slide2text, 'class="redactor shiny"')?>
									<div class="">
										<span class="btn btn-inverse fileinput-button">
											<i class="icon-plus icon-white"></i>
											<span>Select file</span>
											<?=form_upload('slide2image', '', 'id="fileupload"')?>
										</span>
										<div class="clear"></div>
										<span id="filepath"></span>
										<?php if($row->slide2image != '') { ?>
										<img src="<?=substr(base_url(), 0, -6)?>uploads/slir/w400-q90/<?=rawurlencode($row->slide2image)?>" alt="Deepcove EU" id="tn" />
										<?php } ?>
									</div>
								</div>
							</div>
							<!-- Slide 3 -->
							<div class="row">
								<div class="grid1">
									<label>Slide 3</label>
								</div>
								<div class="grid2">
									<?=form_input('title', $row->slide3title, 'class="shiny"')?>
									<?=form_textarea('slide3text', $row->slide3text, 'class="redactor shiny"')?>
									<div class="">
										<span class="btn btn-inverse fileinput-button">
											<i class="icon-plus icon-white"></i>
											<span>Select file</span>
											<?=form_upload('slide3image', '', 'id="fileupload"')?>
										</span>
										<div class="clear"></div>
										<span id="filepath"></span>
										<?php if($row->slide1image != '') { ?>
										<img src="<?=substr(base_url(), 0, -6)?>uploads/slir/w400-q90/<?=rawurlencode($row->slide3image)?>" alt="Deepcove EU" id="tn" />
										<?php } ?>
									</div>
								</div>
							</div>
							<!-- Slide 4 -->
							<div class="row">
								<div class="grid1">
									<label>Slide 4</label>
								</div>
								<div class="grid2">
									<?=form_input('slide4title', $row->slide4title, 'class="shiny"')?>
									<?=form_textarea('slide4text', $row->slide4text, 'class="redactor shiny"')?>
									<div class="">
										<span class="btn btn-inverse fileinput-button">
											<i class="icon-plus icon-white"></i>
											<span>Select file</span>
											<?=form_upload('slide4image', '', 'id="fileupload"')?>
										</span>
										<div class="clear"></div>
										<span id="filepath"></span>
										<?php if($row->slide4image != '') { ?>
										<img src="<?=substr(base_url(), 0, -6)?>uploads/slir/w400-q90/<?=rawurlencode($row->slide4image)?>" alt="Deepcove EU" id="tn" />
										<?php } ?>
									</div>
								</div>
							</div>
							<!-- Slide 5 -->
							<div class="row">
								<div class="grid1">
									<label>Slide 5</label>
								</div>
								<div class="grid2">
									<?=form_input('slide5title', $row->slide5title, 'class="shiny"')?>
									<?=form_textarea('slide5text', $row->slide5text, 'class="redactor shiny"')?>
									<div class="">
										<span class="btn btn-inverse fileinput-button">
											<i class="icon-plus icon-white"></i>
											<span>Select file</span>
											<?=form_upload('slide5image', '', 'id="fileupload"')?>
										</span>
										<div class="clear"></div>
										<span id="filepath"></span>
										<?php if($row->slide5image != '') { ?>
										<img src="<?=substr(base_url(), 0, -6)?>uploads/slir/w400-q90/<?=rawurlencode($row->slide5image)?>" alt="Deepcove EU" id="tn" />
										<?php } ?>
									</div>
								</div>
							</div>
					
							
							<?=form_hidden('id', $row->id)?>
			
														
							<div class="row row-high">
								<div class="grid3 text-right">
									<a href="<?=site_url($this->router->class)?>" class="cancel btn">Cancel</a>
									<?=form_submit('submit', 'Save changes', 'class="submit id="submit"')?>
								</div>
							</div>
							
						</div>
					</form>

				</div>
			</div>
		</div> <!-- /container -->
		 