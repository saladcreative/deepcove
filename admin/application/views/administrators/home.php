
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
				
				<div class="wrapper">
				
					<?php if($this->session->flashdata('msg') != '') {	?>	
					<div class="alert alert-info fade in top-23">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?=$this->session->flashdata('msg')?>
					</div>
					<?php } elseif($this->session->flashdata('msg_err') != '') { ?>
					<div class="alert alert-error fade in top-23">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?=$this->session->flashdata('msg_err')?>
					</div>
					<?php } ?>
					
					
					<div class="whead top-23">
						<strong>Administrators</strong>
						<div class="buttontr"><a href="<?=site_url($this->router->class.'/add')?>" class="btn">Add new administrator</a></div>
					</div>

					<div class="box holder tblr twhite">
						<table width="100%" cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<th class="text-left">First Name</th>
									<th class="text-left">Last Name</th>
									<th class="text-left">Email address</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php if ($query->num_rows() > 0) {
								foreach ($query->result() as $row) { ?>
								<tr>
									<td><?=$this->typography->format_characters($row->firstname)?></td>
									<td><?=$this->typography->format_characters($row->lastname)?></td>
									<td><?=$row->email?></td>
									<td class="action">
										<a href="<?=site_url($this->router->class.'/edit/'.$row->id)?>" class="ablanc tip" href="javascript:;" title="Edit"><i class="icon-pencil"></i></a>
										<a href="<?=site_url($this->router->class.'/editpassword/'.$row->id)?>" class="ablanc tip" href="javascript:;" title="Change password"><i class="icon-cog"></i></a>
										<?php if($row->safe == 0) { ?>
										<a href="#delete<?=$row->id?>" class="ablanc tip" href="javascript:;" title="Delete" data-toggle="modal"><i class="icon-remove"></i></a>
										<!-- Delete Modal -->
										<div id="delete<?=$row->id?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												<h3 id="myModalLabel">Delete administrator?</h3>
											</div>
											<div class="modal-body">
												<p>Are you sure that you want to delete <?=$this->typography->format_characters($row->firstname)?> <?=$this->typography->format_characters($row->lastname)?> permanently?</p>
											</div>
											<div class="modal-footer">
												<?=form_open($this->router->class.'/delete')?>
													<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
													<?=form_hidden('id', $row->id)?>
													<?=form_submit('submit', 'Yes - Delete', 'class="btn btn-danger"')?>
												</form
											</div>
										</div>
										 <!-- Logout Modal -->
										
										
										<?php } ?>
									</td>
								</tr>
								<?php } } else { ?>
								<tr>	
									<td colspan="4">No results found</td>
								</tr>
								<?php }  ?>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div> <!-- /container -->
		