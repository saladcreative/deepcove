
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
				
				<div class="wrapper">
					
					<?=form_open($this->router->class.'/save', 'class="main validatepw"')?>
						
						<div class="whead top-23">
							<strong><?=$formtitle?></strong>
						</div>
						
						<div class="box holder type-tip">
							
							<div class="row">
								<div class="grid1">
									<label>First name:</label>
								</div>
								<div class="grid2">
									<?=form_input('firstname', '', 'class="shiny" autocomplete="off"')?>
								</div>
							</div>
							
							<div class="row">
								<div class="grid1">
									<label>Last name:</label>
								</div>
								<div class="grid2">
									<?=form_input('lastname', '', 'class="shiny" autocomplete="off"')?>
								</div>
							</div>
							
							<div class="row">
								<div class="grid1">
									<label>Email address:</label>
								</div>
								<div class="grid2">
									<?=form_input('email', '', 'class="shiny" autocomplete="off"')?>
								</div>
							</div>
							
							<div class="row">
								<div class="grid1">
									<label>Password:</label>
								</div>
								<div class="grid2">
									<?=form_password('password', '', 'class="shiny" id="password"')?>
								</div>
							</div>
							
							<div class="row">
								<div class="grid1">
									<label>Re-type password:</label>
								</div>
								<div class="grid2">
									<?=form_password('confirm_password', '', 'class="shiny"')?>
								</div>
							</div>
														
							<div class="row row-high">
								<div class="grid3 text-right">
									<a href="<?=site_url($this->router->class)?>" class="cancel btn">Cancel</a>
									<?=form_submit('submit', 'Submit', 'class="submit"')?>
								</div>
							</div>
							
						</div>
					</form>

				</div>
			</div>
		</div> <!-- /container -->
		 