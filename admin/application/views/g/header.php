<!DOCTYPE html>
<!--[if lt IE 9]>      <html lang="en" class="ie"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	
	<!-- meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" >
	
	<!-- favicon -->
	<link rel="shortcut icon" href="<?=base_url()?>inc/img/favicon.png" type="image/png">
	
	<!-- page title -->
	<title><?=$this->config->item('customer name');?> :: Administration :: <?=$title?></title>
	
	<!-- css -->
	<link href="<?=base_url()?>inc/css/bootstrap.css" rel="stylesheet" type="text/css" >
	<link href="<?=base_url()?>inc/css/bootstrap-responsive.css" rel="stylesheet" type="text/css" >
	<link href="<?=base_url()?>inc/css/vendor/circle.hover.css" rel="stylesheet" type="text/css" >
	<link href="<?=base_url()?>inc/css/vendor/fullcalendar.css" rel="stylesheet" type="text/css" >
	<link href="<?=base_url()?>inc/css/vendor/sourcerer.css" rel="stylesheet" type="text/css" >
    <link href="<?=base_url()?>inc/css/vendor/prettify.css" rel="stylesheet" type="text/css" >
    <link href="<?=base_url()?>inc/css/vendor/jquery.mcustomscrollbar.css" rel="stylesheet" type="text/css" >
    <link href="<?=base_url()?>inc/css/vendor/jquery.gritter.css" type="text/css" rel="stylesheet" >
    <link href="<?=base_url()?>inc/css/vendor/jquery-ui.css" type="text/css" rel="stylesheet" >
	<link href="<?=base_url()?>inc/css/vendor/redactor.css" rel="stylesheet" type="text/css" >
	<link href="<?=base_url()?>inc/css/vendor/jquery.tagsinput.css" rel="stylesheet" >
	<link href="<?=base_url()?>inc/css/jquery.fileupload.css" rel="stylesheet">
	<link href="<?=base_url()?>inc/css/main.css" rel="stylesheet" type="text/css" >
	<link href="<?=base_url()?>inc/css/media.css" rel="stylesheet" type="text/css" >

	
	<!-- fonts -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	


</head>
<body>
		<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
		
		<!-- gotop -->
		<div id="gotop"></div>