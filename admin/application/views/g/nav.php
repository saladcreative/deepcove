
		
		<!-- top navigation bar -->
		<span class="accent-1"></span>
		
		<div class="navbar navbar-fixed-top">
		  
			<a href="<?=site_url()?>">
				<div id="logo"></div>
			</a>

			<?php /*
			<div class="search">
				<input type="text" placeholder="Search" maxlength="50">
			</div>
			
			<!-- header navigation -->
			<nav class="nav">
				<ul>
					<li><a href="#">Administrators</a></li>
					<li><a href="#">User profile</a></li>
					<li><a href="#">Permissions</a></li>
					<li><a href="#">More options</a></li>
				</ul>
			</nav>
			
			*/ ?>
			
			<!-- right part header icons -->
			<ul class="user-nav">
				<?php //<li><a href="#" data-placement="bottom" class="icon-status tip" title="Check stats"></a></li> ?>
				<li><a href="<?=site_url('administrators')?>" data-placement="bottom" class="icon-user tip" title="Administrators"></a></li>
				<?php //<li><a href="#" data-placement="bottom" class="icon-dialog tip" title="Some information"></a></li> ?>
				<li><a href="#logout" data-placement="bottom" class="icon-logout tip" title="Logout" data-toggle="modal"></a></li>
			</ul>
			
			
			
		 
		</div>
		
		
		<!-- sidebar -->
		<div class="sidebar-nav">
			
			<!-- sidebar navigation -->
			<nav id="navigation">
				<ul>
					<li><a <?php if($nav == 'dashboard') { echo 'class="active" '; } ?>href="<?=site_url()?>">Dashboard</a></li>
					<li>
						<a <?php if($nav == 'home_model') { echo 'class="active" '; } ?>href="<?=site_url('homepage')?>">Home page</a>
					</li>
					<li>
						<a <?php if($nav == 'info_model') { echo 'class="active" '; } ?>href="<?=site_url('info')?>">Info pages</a>
					</li>
					<li><a <?php if($nav == 'optiswiss_lenses') { echo 'class="active" '; } ?>href="<?=site_url('optiswiss_lenses')?>">Optiswiss/Deepcove Landing pages</a>
					</li>
					<li>
						<a <?php if($nav == 'enquiries') { echo 'class="active" '; } ?>href="<?=site_url('enquiries')?>">Contact Page Enquiries</a>
					</li>
<!-- 					<li><a <?php if($nav == 'pages') { echo 'class="active" '; } ?>href="<?=site_url('pages')?>">All Pages / SEO</a>
					</li> -->
				</ul>
			</nav>
		</div>
