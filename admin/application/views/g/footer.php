		 <!-- Logout Modal -->
		<div id="logout" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">Log out?</h3>
			</div>
			<div class="modal-body">
				<p>Are you sure that you want to log out from the administrator area?</p>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">No - cancel</button>
				<a href="<?=site_url('auth/logout')?>" class="btn btn-primary">Yes - log me out</a>
			</div>
		</div>
		 <!-- Logout Modal -->
		
		<?php /*		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/bootstrap.min.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/tweenLite/TweenMax.min.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/jquery.sparkline.min.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/prettify.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/excanvas.min.js"></script>
        
        <script src="<?=base_url()?>inc/js/vendor/jquery.flot.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/jquery.flot.resize.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/jquery.flot.tooltip.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/fullcalendar.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/beautiful-data.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/jquery.mcustomscrollbar.concat.min.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/jquery.gritter.min.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/jquery.tagsinput.min.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/redactor/redactor.min.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/jquery.validate.js"></script>
        <script src="<?=base_url()?>inc/js/vendor/jquery.customselect.min.js"></script>
        <script src="<?=base_url()?>inc/js/main.js"></script>
        <?php if(isset($js)) { ?><script src="<?=base_url()?>inc/js/page/<?=$js?>.js"></script><?php } ?>
        
         */ ?>
        
    	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>inc/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>inc/js/tweenLite/TweenMax.min.js"></script>
	    <script type="text/javascript" src="<?=base_url()?>inc/js/jquery.sparkline.min.js"></script>
	    <script type="text/javascript" src="<?=base_url()?>inc/js/prettify.js"></script>
	    <script type="text/javascript" src="<?=base_url()?>inc/js/excanvas.min.js"></script>
	    
	    <script type="text/javascript" src="<?=base_url()?>inc/js/fullcalendar.min.js"></script>
	    <script type="text/javascript" src="<?=base_url()?>inc/js/beautiful-data.min.js"></script>
	    
	    <script type="text/javascript" src="<?=base_url()?>inc/js/jquery.flot.js"></script>
	    <script type="text/javascript" src="<?=base_url()?>inc/js/jquery.flot.resize.js"></script>
	    <script type="text/javascript" src="<?=base_url()?>inc/js/jquery.flot.tooltip.js"></script>
	    	    
	    <script type="text/javascript" src="<?=base_url()?>inc/js/jquery.mcustomscrollbar.concat.min.js"></script>
	    <script type="text/javascript" src="<?=base_url()?>inc/js/jquery.gritter.min.js"></script>
	    
		<script type="text/javascript" src="<?=base_url()?>inc/js/redactor.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>inc/js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>inc/js/jquery.tagsinput.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>inc/js/jquery.customselect.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>inc/js/jquery.radiostyling.js"></script>
		<script type="text/javascript" src="<?=base_url()?>inc/js/main.js"></script>
        
    </body>
</html>