
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
				
				<div class="wrapper">
				
					<?php if($this->session->flashdata('msg') != '') {	?>	
					<div class="alert alert-info fade in top-23">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?=$this->session->flashdata('msg')?>
					</div>
					<?php } if($this->session->flashdata('msg_err') != '') { ?>
					<div class="alert alert-error fade in top-23">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?=$this->session->flashdata('msg_err')?>
					</div>
					<?php } ?>
					
					
					<div class="whead top-23">
						<strong>Landing Pages</strong>
						<div class="buttontr">&nbsp;</div>
					</div>
					
					<div class="table-search-holder sizing">
						<input type="text" class="table-search" placeholder="Search">
					</div>

					<div class="box holder tblr twhite">
						<table width="100%" cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<th>Page</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php if ($query->num_rows() > 0) {
								foreach ($query->result() as $row) { ?>
								<tr>
									
									<td><?=$this->typography->format_characters($row->title)?></td>
									<td class="action">
										<a href="<?=site_url($this->router->class.'/edit/'.$row->id)?>" class="ablanc tip" href="javascript:;" title="Edit"><i class="icon-pencil"></i></a>	
										<a href="<?=site_url($this->router->class.'/block/'.$row->id)?>" class="ablanc tip" href="javascript:;" title="Resources"><i class="icon-arrow-right"></i></a>			
									</td>
								</tr>
								<?php } } else { ?>
								<tr>	
									<td colspan="4">No results found</td>
								</tr>
								<?php }  ?>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div> <!-- /container -->
		