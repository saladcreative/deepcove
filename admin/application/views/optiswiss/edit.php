
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
				
				<div class="wrapper">
					
					<?=form_open_multipart($this->router->class.'/save', 'class="main validateform"')?>
						
						<div class="whead top-23">
							<strong><?=$formtitle?></strong>
						</div>
						
						<div class="box holder type-tip">
							
							<div class="row">
								<div class="grid1">
									<label>Page Title</label>
								</div>
								<div class="grid2">
									<?=form_input('title', $row->title, 'class="shiny"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>Page Title (SEO):</label>
								</div>
								<div class="grid2">
									<?=form_input('pagetitle', $row->title, 'class="shiny" autocomplete="off"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>META Description (SEO):</label>
								</div>
								<div class="grid2">
									<?=form_textarea('metadesc', $row->metadesc, 'class="shiny" autocomplete="off"')?>
								</div>
							</div>
							
							<?=form_hidden('id', $row->id)?>
			
														
							<div class="row row-high">
								<div class="grid3 text-right">
									<a href="<?=site_url($this->router->class)?>" class="cancel btn">Cancel</a>
									<?=form_submit('submit', 'Save changes', 'class="submit id="submit"')?>
								</div>
							</div>
							
						</div>
					</form>

				</div>
			</div>
		</div> <!-- /container -->
		 