<div class="container">
	
	<div id="title">
		<?=$this->config->item('customer name');?> :: <?=$title?>
	</div>
	
	<div id="content">
		
		<div class="wrapper">
				
			<div class="whead top-23">
				<strong><?=$formtitle?></strong>
			</div>
			
			<div class="box holder type-tip">
				<div class="main">
				<?php foreach ($query->result() as $row) { ?>
					<div class="row">
						<div class="grid1">
							<label><?php echo $row->blocktitle ?></label>
						</div>
						<div class="grid2">
							<label class="float-right text-right" style="text-align:right;float:right;">
								<a href="<?=site_url($this->router->class.'/row/'.$row->id)?>">Edit</a>
							</label>
						</div>
					</div>
				<?php } ?>
				</div>

				<!-- <?=form_hidden('id', $row->id)?> -->

			</div>
		</form>

	</div>
</div>
</div> <!-- /container -->