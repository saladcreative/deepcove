<div class="container">
			
	<div id="title">
		<?=$this->config->item('customer name');?>
	</div>
	
	<div id="content">
		
		<div class="wrapper">
			
			<div class="not-found">
				
				<div class="note">
					<span>404</span>
					<strong>Page not found</strong>
				</div>
				
			</div>
			
		</div>
	</div>
</div> <!-- /container -->