
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
				
				<div class="wrapper">
					
					<?=form_open_multipart($this->router->class.'/save', 'class="main validateform"')?>
						
						
						 <div class="whead top-23">
							<strong><?=$formtitle?></strong>
						</div>
						
						<div class="box holder type-tip">
							
							<div class="row">
								<div class="grid1">
									<label>Page Name:</label>
								</div>
								<div class="grid2">
									<?=form_input('pagename', $row->pagename, 'id="titlefield" class="shiny required" disabled autocomplete="off"')?>
								</div>
							</div>

							<?php if($row->seoonly == 0) { ?>
							
							<div class="row">
								<div class="grid1">
									<label>Content:</label>
								</div>
								<div class="grid2">
									<?=form_textarea('text', $row->text, 'class="shiny redactor"')?>
								</div>
							</div>
														
							<?php } ?>

							<div class="row">
								<div class="grid1">
									<label>Page Title (SEO):</label>
								</div>
								<div class="grid2">
									<?=form_input('pagetitle', $row->pagetitle, 'class="shiny" autocomplete="off"')?>
								</div>
							</div>
							
							<div class="row">
								<div class="grid1">
									<label>META Description (SEO):</label>
								</div>
								<div class="grid2">
									<?=form_input('metadesc', $row->metadesc, 'class="shiny" autocomplete="off"')?>
								</div>
							</div>
							
							<?=form_hidden('id', $row->id)?>
							<?=form_hidden('seoonly', $row->seoonly)?>
														
							<div class="row row-high">
								<div class="grid2">
								<p class="toppad"><?php if($row->updated != '') { ?>
									Last updated <?=date('d/m/Y (g:ia)', strtotime($row->updated))?> by <?=$row->updatedby?>
								<?php } ?></p>
								</div>
								<div class="grid1 text-right">
									<a href="<?=site_url($this->router->class)?>" class="cancel btn">Cancel</a>
									<?=form_submit('submit', 'Save changes', 'class="submit id="submit"')?>
								</div>
							</div>
							
						</div>
					</form>

				</div>
			</div>
		</div> <!-- /container -->
		 