
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
				
				<div class="wrapper">
					
					<?=form_open_multipart($this->router->class.'/savecat', 'class="main validateform"')?>
						
						<div class="whead top-23">
							<strong><?=$formtitle?></strong>
						</div>
						
						<div class="box holder type-tip">
							
							<div class="row">
								<div class="grid1">
									<label>Category Name:</label>
								</div>
								<div class="grid2">
									<?=form_input('cat_title', $row->cat_title, 'class="shiny required" autocomplete="off"')?>
								</div>
							</div>
						
							<?=form_hidden('cat_id', $row->cat_id)?>
																					
							<div class="row row-high">
								<div class="grid3 text-right">
									<a href="<?=site_url($this->router->class)?>/categories" class="cancel btn">Cancel</a>
									<?=form_submit('submit', 'Submit', 'class="submit id="submit"')?>
								</div>
							</div>
							
						</div>
					</form>

				</div>
			</div>
		</div> <!-- /container -->
		 