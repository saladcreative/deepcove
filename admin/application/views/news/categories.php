
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
			
				<div class="invlice-top max-900 auto">
						
					</div>
				
				<div class="wrapper">
				
					<?php if($this->session->flashdata('msg') != '') {	?>	
					<div class="alert alert-info fade in top-23">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?=$this->session->flashdata('msg')?>
					</div>
					<?php } if($this->session->flashdata('msg_err') != '') { ?>
					<div class="alert alert-error fade in top-23">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?=$this->session->flashdata('msg_err')?>
					</div>
					<?php } ?>
					
					
					
					
					<div class="whead top-23">
						<strong>Categories </strong>
						<div class="buttontr"><a href="<?=site_url($this->router->class.'/addcat')?>" class="btn" >Add new category</a></div>
					</div>
				

					<div class="box holder tblr twhite">
						<table width="100%" cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<th>Category name</th>
									<th>No. of articles in category</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php if ($query->num_rows() > 0) {
								foreach ($query->result() as $row) { ?>
								<?php $noarticles = $this->model->no_articles_in_cat($row->cat_id); ?>
								<tr>
									
									<td><?=$this->typography->format_characters($row->cat_title)?></td>
									<td><?=$noarticles?></td>
									<td class="action">
										
										<a href="<?=site_url($this->router->class.'/editcat/'.$row->cat_id)?>" class="ablanc tip" href="javascript:;" title="Edit"><i class="icon-pencil"></i></a>
										<?if($noarticles == 0) { ?><a href="#delete<?=$row->cat_id?>" class="ablanc tip" href="javascript:;" title="Delete" data-toggle="modal"><i class="icon-remove"></i></a>
										<!-- Delete Modal -->
										<div id="delete<?=$row->cat_id?>" class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												<h3 id="myModalLabel">Delete category?</h3>
											</div>
											<div class="modal-body">
												<p>Are you sure that you want to delete the '<?=$this->typography->format_characters($row->cat_title)?>' category permanently?</p>
											</div>
											<div class="modal-footer">
												<?=form_open($this->router->class.'/deletecat')?>
													<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
													<?=form_hidden('cat_id', $row->cat_id)?>
													<?=form_submit('submit', 'Yes - Delete', 'class="btn btn-danger"')?>
												</form
											</div>
										</div>
										<!-- Delete Modal -->
										<?php } ?>
									</td>
								</tr>
								<?php } } else { ?>
								<tr>	
									<td colspan="4">No results found</td>
								</tr>
								<?php }  ?>
							</tbody>
						</table>
						
						
						
						
					</div>

				</div>
			</div>
		</div> <!-- /container -->
		