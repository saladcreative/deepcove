
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
				
				<div class="wrapper">
					
					<?=form_open_multipart($this->router->class.'/save', 'class="main validateform"')?>
						
						<div class="whead top-23">
							<strong><?=$formtitle?></strong>
						</div>
						
						<div class="box holder type-tip">
							
							<div class="row">
								<div class="grid1">
									<label>Title:</label>
								</div>
								<div class="grid2">
									<?=form_input('title', '', 'id="titlefield" class="shiny required" autocomplete="off"')?>
								</div>
							</div>
							
							<div class="row">
								<div class="grid1">
									<label>Permalink:</label>
								</div>
								<div class="grid2 slugwrapper">
									<p><?=substr(base_url(), 0, -6)?>blog/<span id="slug"></span></p>
									<?=form_hidden('slug')?>
								</div>
							</div>
							
							<div class="row">
								<div class="grid1">
									<label>Date:
									<span class="small">DD/MM/YYYY format</span></label>
								</div>
								<div class="grid2">
									<?=form_input('date', date('j/m/Y'), 'class="shiny datepicker required"')?>
								</div>
							</div>
							
							<div class="row">
								<div class="grid1">
									<label>Image:
									<span class="small">Min image width 650px<br>Max file size 10mb<br>JPG or PNG files only</span></label>
								</div>
								<div class="grid2">
									 <span class="btn btn-inverse fileinput-button">
								        <i class="icon-plus icon-white"></i>
								        <span>Select file</span>
								        <?=form_upload('img', '', 'id="fileupload"')?>
								    </span>
								    <div class="clear"></div>
								    <span id="filepath"></span>
								</div>
							</div>

							<div class="row">
								<div class="grid1">
									<label>Article:</label>
								</div>
								<div class="grid2">
									<?=form_textarea('text', '', 'class="shiny redactor"')?>
								</div>
							</div>
							
							
							<div class="row">
								<div class="grid1">
									<label>Category:</label>
								</div>
								<div class="grid2">
									<?=form_dropdown('category', $categories, '', 'class="styled required"')?>
									
								</div>
							</div>
							
							<div class="row">
								<div class="grid1">
									<label>Tags:
									<span class="small">Separate with commas</span></label>
								</div>
								<div class="grid2">
									<?=form_input('tags', '', 'class="counter shiny" id="tags"')?>
								</div>
							</div>
							
							<div class="row">
								<div class="grid1">
									<label>Publish:</label>
								</div>
								<div class="grid2">
									<?=form_checkbox('publish', 1, TRUE)?>
								</div>
							</div>
			
														
							<div class="row row-high">
								<div class="grid3 text-right">
									<a href="<?=site_url($this->router->class)?>" class="cancel btn">Cancel</a>
									<?=form_submit('submit', 'Submit', 'class="submit id="submit"')?>
								</div>
							</div>
							
						</div>
					</form>

				</div>
			</div>
		</div> <!-- /container -->
		 