<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" >
	
	<title><?=$this->config->item('customer name');?> :: Administration :: Log in</title>
	
	<link href="<?=base_url()?>inc/css/bootstrap.css" rel="stylesheet" type="text/css" >
	<link href="<?=base_url()?>inc/css/bootstrap-responsive.css" rel="stylesheet" type="text/css" >
	<link href="<?=base_url()?>inc/css/main.css" rel="stylesheet" type="text/css" >
	
	<!-- fonts -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>

</head>
<body class="login">
		<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
		
		<!-- login form -->
		<?=form_open('auth/login', 'class="form-signin"')?>
			<div class="profile">
				<span><img src="<?=base_url()?>inc/img/logo.png" alt=""></span>
			</div>
			<input type="text" name="email" class="input-block-level email required" placeholder="Email address" autocomplete="off">
			<input type="password" name="password" class="input-block-level password required" placeholder="Password">
			
			<div class="login-bottom">
				<?php if($this->session->flashdata('msg') != '') { ?><div class="loginwarning">Username or password not recognised</div><?php } ?>
				<button class="btn btn-large btn-primary" type="submit">Login</button>
			</div>
		</form>
		
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
        <script src="<?=base_url()?>inc/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>inc/js/tweenLite/TweenMax.min.js"></script>
        <script src="<?=base_url()?>inc/js/prettify.js"></script>
        <script src="<?=base_url()?>inc/js/jquery.sparkline.min.js"></script>
        <script src="<?=base_url()?>inc/js/jquery.mcustomscrollbar.concat.min.js"></script>
        <script src="<?=base_url()?>inc/js/login.js"></script>

		
    </body>
</html>
