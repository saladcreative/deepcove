							
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
				
				<div class="wrapper">
					
					<?=form_open_multipart($this->router->class.'/save_row', 'class="main validateform"')?>
						
						<div class="whead top-23">
							<strong><?=$formtitle?></strong>
						</div>
						
						<div class="box holder type-tip">
							<div class="row">
								<div class="grid1">
									<label>Block Title</label>
								</div>
								<div class="grid2">
									<?=form_input('blocktitle', $row->blocktitle, 'class="shiny"')?>
								</div>
							</div>

							<div class="row">
								<div class="grid1">
									<label>Block Body</label>
								</div>
								<div class="grid2">
									<?=form_textarea('blockbody', $row->blockbody, 'class="shiny"')?>
								</div>
							</div>		

							<div class="row">
								<div class="grid1">
									<label>Image:
									<span class="small">Minimum width: 1500px<br>Max file size 5mb<br>JPG or PNG files only</span></label>
								</div>
								<div class="grid2">
									 <span class="btn btn-inverse fileinput-button">
								        <i class="icon-plus icon-white"></i>
								        <span>Select file</span>
								        <?=form_upload('blockimage', '', 'id="fileupload"')?>
								    </span>
								    <div class="clear"></div>
								    <span id="filepath"></span>
								    <?php if($row->blockimage != '') { ?>
								    <img src="<?=substr(base_url(), 0, -6)?>uploads/slir/w400-q90/<?=rawurlencode($row->blockimage)?>" alt="Deepcove EU" id="tn" />
								    <?php } ?>
								</div>
							</div>

							<div class="row">
								<div class="grid1">
									<label>Link 2</label>
								</div>
								<div class="grid2">
									<?=form_input('link2', $row->link_2, 'class="shiny"')?>
								</div>
							</div>
						

							<?=form_hidden('id', $row->id)?>
			
														
							<div class="row row-high">
								<div class="grid3 text-right">
									<a href="<?=site_url($this->router->class)?>" class="cancel btn">Cancel</a>
									<?=form_submit('submit', 'Save changes', 'class="submit id="submit"')?>
								</div>
							</div>
							
						</div>
					</form>

				</div>
			</div>
		</div> <!-- /container -->