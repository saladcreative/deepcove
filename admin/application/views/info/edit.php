
		<div class="container">
			
			<div id="title">
				<?=$this->config->item('customer name');?> :: <?=$title?>
			</div>
			
			<div id="content">
				
				<div class="wrapper">
					
					<?=form_open_multipart($this->router->class.'/save', 'class="main validateform"')?>
						
						<div class="whead top-23">
							<strong><?=$formtitle?></strong>
						</div>
						
						<div class="box holder type-tip">

							<div class="row">
								<div class="grid1">
									<label>Page Heading</label>
								</div>
								<div class="grid2">
									<?=form_input('title', $row->title, 'class="shiny"')?>
								</div>
							</div>

							<div class="row">
								<div class="grid1">
									<label>Page Title (SEO):</label>
								</div>
								<div class="grid2">
									<?=form_input('seotitle', $row->seotitle, 'class="shiny" autocomplete="off"')?>
								</div>
							</div>

							<div class="row">
								<div class="grid1">
									<label>First section</label>
								</div>
								<div class="grid2">
									<?=form_textarea('firstheading', $row->firstheading, 'class="redactor shiny"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>Second section</label>
								</div>
								<div class="grid2">
									<?=form_textarea('firstsection', $row->firstsection, 'class="redactor shiny"')?>
								</div>
							</div>

							<div class="row">
								<div class="grid1">
									<label>Third section</label>
								</div>
								<div class="grid2">
									<?=form_textarea('secondheading', $row->secondheading, 'class="redactor shiny"')?>
								</div>
							</div>
							<div class="row">
								<div class="grid1">
									<label>Fourth section</label>
								</div>
								<div class="grid2">
									<?=form_textarea('secondsection', $row->secondsection, 'class="redactor shiny"')?>
								</div>
							</div>

							<div class="row">
								<div class="grid1">
									<label>META Description (SEO):</label>
								</div>
								<div class="grid2">
									<?=form_input('metadesc', $row->metadesc, 'class="shiny" autocomplete="off"')?>
								</div>
							</div>

							<div class="row">
								<div class="grid1">
									<label>PDF Upload (optional):
									<span class="small">>Max file size 5mb<br>PDF files only</span></label>
								</div> 
								<div class="grid2">
									 <span class="btn btn-inverse fileinput-button">
								        <i class="icon-plus icon-white"></i>
								        <span>Select file</span>
								        <?=form_upload('pdfupload', '', 'id="fileupload"')?>
								    </span>
								    <div class="clear"></div>
								    <span id="filepath"></span>
								    <?php if($row->pdf_upload != '') { ?>
								    <img src="<?=substr(base_url(), 0, -6)?>uploads/slir/w400-q90/<?=rawurlencode($row->image)?>" alt="Deepcove EU" id="tn" />
								    <?php } ?>
								</div>
							</div>


							
							<?=form_hidden('id', $row->id)?>
			
														
							<div class="row row-high">
								<div class="grid3 text-right">
									<a href="<?=site_url($this->router->class)?>" class="cancel btn">Cancel</a>
									<?=form_submit('submit', 'Save changes', 'class="submit id="submit"')?>
								</div>
							</div>
							
						</div>
					</form>

				</div>
			</div>
		</div> <!-- /container -->
		 