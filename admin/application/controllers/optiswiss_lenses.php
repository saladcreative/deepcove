<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Optiswiss_lenses extends CI_Controller {

	
	public function __construct()
	{
	    parent::__construct();
	    
	    if($this->session->userdata('logged') != TRUE) { 	
			redirect('auth/login');
		}
		
		$this->load->model('optiswiss_model', 'model');
	}
	
	public function index()
	{
		//get data
		$d['query'] = $this->model->return_all();
		
		//define page title, custom js and active nav
		$d['title'] = 'Landing Pages';
		$d['js'] = 'optiswisslenses';
		$d['nav'] = 'optiswisslenses';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('optiswiss/home', $d);
		$this->load->view('g/footer', $d);
	}

	public function edit()
	{
		
		$query = $this->model->return_one_by_id($this->uri->segment(3));
		if ($query->num_rows() == 0) { show_404(); }
		
		$row = $query->row();
		$d['row'] = $row; 	
	
		//define page title, custom js and active nav
		$d['title'] = 'Landing Pages :: Edit';
		$d['formtitle'] = 'Edit page';
		$d['js'] = 'implants';
		$d['nav'] = 'implants';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('optiswiss/edit', $d);
		$this->load->view('g/footer', $d);
		
	}

	public function save()
	{
		
		// Save
		$this->model->save();
		redirect($this->router->class);
		
	}

	public function block()
	{

		//get data
		$d['query'] = $this->model->get_blocks($this->uri->segment(3));	
		$d['formtitle'] = 'All blocks';

		
		//define page title, custom js and active nav
		$d['title'] = 'Edit Individual Row';
		$d['js'] = 'row';
		$d['nav'] = 'row';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('optiswiss/block', $d);
		$this->load->view('g/footer', $d);
		
	}

	public function row()
	{

		//get data
		$query = $this->model->get_info_blocks($this->uri->segment(3));
		if ($query->num_rows() == 0) { show_404(); }
		
		$row = $query->row();
		$d['row'] = $row; 	
		$d['formtitle'] = 'Edit row';
		//define page title, custom js and active nav
		$d['title'] = 'Edit Individual Row';
		$d['js'] = 'row';
		$d['nav'] = 'row';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('optiswiss/row', $d);
		$this->load->view('g/footer', $d);
		
	}

	public function save_row()
	{

		// Save
		$this->model->save_row();
		redirect($this->router->class);
		
	}


}