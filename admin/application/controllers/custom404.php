<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom404 extends CI_Controller {

	
	public function index()
	{
		$this->output->set_status_header('404');
		//$d['js'] = 'dashboard';
		
		$d['title'] = '404 - Page not found';
		$d['nav'] = '';
		
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('404', $d);
		$this->load->view('g/footer');
	}
	
}

