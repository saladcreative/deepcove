<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrators extends CI_Controller {


	public function __construct()
	{
	    parent::__construct();
	    
	    if($this->session->userdata('logged') != TRUE) { 	
			redirect('auth/login');
		}
		
		$this->load->model('administrators_model', 'model');
		
		
	}
	
	public function index()
	{
		
		//get data
		$d['query'] = $this->model->return_all('firstname');
		
		//define page title, custom js and active nav
		$d['title'] = 'Administrators';
		$d['js'] = 'administrators';
		$d['nav'] = 'administrators';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('administrators/home', $d);
		$this->load->view('g/footer', $d);
		
	}
	
	public function add()
	{

		//define page title, custom js and active nav
		$d['title'] = 'Administrators :: Add';
		$d['formtitle'] = 'Add new administrator';
		$d['js'] = 'administrators';
		$d['nav'] = 'administrators';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('administrators/add', $d);
		$this->load->view('g/footer', $d);
		
	}
	
	public function edit()
	{
		
		$query = $this->model->return_one_by_id($this->uri->segment(3));
		if ($query->num_rows() == 0) { show_404(); }
		
		$row = $query->row();
		$d['row'] = $row; 	
		
		//define page title, custom js and active nav
		$d['title'] = 'Administrators :: Edit';
		$d['formtitle'] = 'Edit administrator';
		$d['js'] = 'administrators';
		$d['nav'] = 'administrators';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('administrators/edit', $d);
		$this->load->view('g/footer', $d);
		
	}
	
	public function editpassword()
	{
		
		$query = $this->model->return_one_by_id($this->uri->segment(3));
		if ($query->num_rows() == 0) { show_404(); }
		
		$row = $query->row();
		$d['row'] = $row; 	
		
		//define page title, custom js and active nav
		$d['title'] = 'Administrators :: Change password';
		$d['formtitle'] = 'Change password';
		$d['js'] = 'administrators';
		$d['nav'] = 'administrators';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('administrators/editpassword', $d);
		$this->load->view('g/footer', $d);
		
	}
	
	public function save()
	{
		
		// Save user
		$this->model->save();
		redirect($this->router->class);
		
	}
	
	public function delete()
	{
		if($this->input->post('id')) {
			$this->model->delete($this->input->post('id'));
			$this->session->set_flashdata('msg', 'Administrator successfully deleted');
		} else {
			$this->session->set_flashdata('msg_err', '<strong>Whoops!</strong> Something went wrong. Please try again.');
		}
		
		redirect($this->router->class);
		
	}
	
}

