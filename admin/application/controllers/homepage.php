<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends CI_Controller {

	
	public function __construct()
	{
	    parent::__construct();
	    
	    if($this->session->userdata('logged') != TRUE) { 	
			redirect('auth/login');
		}
		
		$this->load->model('home_model', 'model');
	}
	
	public function index()
	{
		//get data
		$d['query'] = $this->model->return_all();
		
		//define page title, custom js and active nav
		$d['title'] = 'Home page';
		$d['js'] = 'homepage';
		$d['nav'] = 'homepage';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('homepage/home', $d);
		$this->load->view('g/footer', $d);
	}

	public function edit()
	{
		
		$query = $this->model->return_one_by_id($this->uri->segment(3));
		if ($query->num_rows() == 0) { show_404(); }
		
		$row = $query->row();
		$d['row'] = $row; 	
	
		//define page title, custom js and active nav
		$d['title'] = 'Homepage :: Edit';
		$d['formtitle'] = 'Edit page';
		$d['js'] = 'implants';
		$d['nav'] = 'implants';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('homepage/edit', $d);
		$this->load->view('g/footer', $d);
		
	}

	public function save()
	{
		
		// Save
		$this->model->save();
		redirect($this->router->class);
		
	}

}