<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {


	public function __construct()
	{
	    parent::__construct();
	    
	    if($this->session->userdata('logged') != TRUE) { 	
			redirect('auth/login');
		}
	}
	
	public function index()
	{
		$d['title'] = 'Dashboard';
		$d['js'] = 'dashboard';
		$d['nav'] = 'dashboard';
		
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('dashboard', $d);
		$this->load->view('g/footer', $d);
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */