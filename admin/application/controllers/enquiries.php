<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Enquiries extends CI_Controller {

	
	public function __construct()
	{
	    parent::__construct();
	    
	    if($this->session->userdata('logged') != TRUE) { 	
			redirect('auth/login');
		}
		
		$this->load->model('enquiries_model', 'model');
		
	}
	
	public function index()
	{
		
		//get data
		$d['query'] = $this->model->return_all();
		
		//define page title, custom js and active nav
		$d['title'] = 'Contact Enquiries';
		$d['js'] = 'enquiries';
		$d['nav'] = 'enquiries';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('enquiries/home', $d);
		$this->load->view('g/footer', $d);
		
	}

	
	public function delete()
	{
		if($this->input->post('id')) {
			$this->model->delete($this->input->post('id'));
			$this->session->set_flashdata('msg', 'Entry successfully deleted');
		} else {
			$this->session->set_flashdata('msg_err', '<strong>Whoops!</strong> Something went wrong. Please try again.');
		}
		
		redirect($this->router->class);
		
	}

	public function export() 
    {
        $this->load->dbutil();
        $this->load->helper('download');
        $query = $this->db->query("SELECT * FROM enquiries");
        $delimiter = ",";
        $newline = "\r\n";
        $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        force_download('export.csv', $data);
    }

	

}

