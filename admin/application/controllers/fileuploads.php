<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fileuploads extends CI_Controller {


	public function __construct()
	{
	    parent::__construct();
	    
	    if($this->session->userdata('logged') != TRUE) { 	
			redirect('auth/login');
		}
	}
	
	public function do_upload_img()
	{
		
		$config['upload_path'] = '../uploads/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file')) {
		
			$errors = $this->upload->display_errors();
			log_message('error', '../uploads/');
			log_message('error', print_r($errors, true));

			//$this->load->view('upload_form', $error);
			
		} else {
		
			$data = $this->upload->data();

			echo stripslashes(json_encode(array('filelink' => site_url('../uploads/'.$data['file_name']))));
		
		}
						
	}
	
	public function do_upload_file()
	{
		
		$config['upload_path'] = '../uploads/';
		$config['allowed_types'] = 'doc|docx|ppt|pptx|pdf|xls|xlsx|rtf|txt';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file')) {
		
			$errors = $this->upload->display_errors();
			log_message('error', '../uploads/');
			log_message('error', print_r($errors, true));

			//$this->load->view('upload_form', $error);
			
		} else {
		
			$data = $this->upload->data();

			echo stripslashes(json_encode(array('filelink' => site_url('../uploads/'.$data['file_name']))));
		
		}
						
	}
	
	
	
}

