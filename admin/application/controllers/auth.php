<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {



	public function index(){
		
		if(!$this->session->userdata('loggedin'))
		{
			redirect('auth/login');
		}
		else
		{
			$this->load->view('login');
		}
		
	}

	public function login()
	{
		
		if($this->input->post('email') != '')
		{
			// form submitted
			$arr = array(
				'email' => $this->input->post('email'),
				'password' => sha1($this->input->post('password'))
			);

			
			$d = $this->db->limit(1)->get_where('administrators', $arr);
				
			if($d->num_rows() > 0)
			{
				$r = $d->row();
				$arr = array(
						'sess_expiration' => 25200,
						'logged' => TRUE,
						'firstname' => $r->firstname,
						'lastname' => $r->lastname,
						'email' => $r->email,
						'admin' => '1',
						'safe' => $r->safe,
						'sess_cookie_name' => 'norfolkadmincookie'
					);
					
				$this->session->set_userdata($arr);
				
				$this->session->set_flashdata('msg', 'You have logged in successfully');
				
				redirect('home');
				
			}
			else
			{
				$this->session->set_userdata('logged', FALSE);
				
				$this->session->set_flashdata('msg', 'Incorrect details entered. Please try again');
				redirect('auth/login/');
			}

		}
		else
		{
			$this->session->set_flashdata('msg', 'Incorrect details entered. Please try again');
			$this->load->view('login');
		}
		
		
	}
	
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('auth/login');
	}
	


}
?>