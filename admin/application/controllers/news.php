<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	
	public function __construct()
	{
	    parent::__construct();
	    
	    if($this->session->userdata('logged') != TRUE) { 	
			redirect('auth/login');
		}
		
		$this->load->model('news_model', 'model');
		
	}
	
	public function index()
	{
		
		//get data
		$d['query'] = $this->model->return_all_news();
		
		//define page title, custom js and active nav
		$d['title'] = 'Blog';
		$d['js'] = 'news';
		$d['nav'] = 'news';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('news/home', $d);
		$this->load->view('g/footer', $d);
		
	}
	
	public function add()
	{
	
		//get categories for drop down
		$catquery = $this->model->get_all_news_categories();
		
        if($catquery->num_rows() > 0) {
	        $cats = array();
			$cats[''] = '- Please Select -';
			foreach ($catquery->result_array() as $row) {
			  $cats[$row['cat_id']] = $row['cat_title'];
			}
		} else {
			$cats = array();
			$cats[''] = '- NO CATEGORIES AVAILABLE -';
		}
		
		$d['categories'] = $cats;
		//get categories for drop down

		//define page title, custom js and active nav
		$d['title'] = 'Blog :: Add';
		$d['formtitle'] = 'Add new article';
		$d['js'] = 'news';
		$d['nav'] = 'news';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('news/add', $d);
		$this->load->view('g/footer', $d);
		
	}
	
	public function edit()
	{
		
		$query = $this->model->return_one_by_id($this->uri->segment(3));
		if ($query->num_rows() == 0) { show_404(); }
		
		$row = $query->row();
		$d['row'] = $row; 	
		
		//get categories for drop down
		$catquery = $this->model->get_all_news_categories();
		
        if($catquery->num_rows() > 0) {
	        $cats = array();
			$cats[''] = '- Please Select -';
			foreach ($catquery->result_array() as $row) {
			  $cats[$row['cat_id']] = $row['cat_title'];
			}
		} else {
			$cats = array();
			$cats[''] = '- NO CATEGORIES AVAILABLE -';
		}
		
		$d['categories'] = $cats;
		//get categories for drop down
		
		//get tags
		$d['tags'] = $this->model->get_tags($this->uri->segment(3));
		//get tags
		
		
		
		
		//define page title, custom js and active nav
		$d['title'] = 'Blog :: Edit';
		$d['formtitle'] = 'Edit article';
		$d['js'] = 'news';
		$d['nav'] = 'news';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('news/edit', $d);
		$this->load->view('g/footer', $d);
		
	}
	
	public function save()
	{
		
		// Save user
		$this->model->save();
		redirect($this->router->class);
		
	}
	
	public function delete()
	{
		if($this->input->post('id')) {
			$this->model->delete($this->input->post('id'));
			$this->session->set_flashdata('msg', 'Article successfully deleted');
		} else {
			$this->session->set_flashdata('msg_err', '<strong>Whoops!</strong> Something went wrong. Please try again.');
		}
		
		redirect($this->router->class);
		
	}
	
	
	
	
	public function categories()
	{
		
		//get data
		$d['query'] = $this->model->get_all_news_categories();
		
		//define page title, custom js and active nav
		$d['title'] = 'Blog :: categories';
		$d['js'] = 'news';
		$d['nav'] = 'news_categories';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('news/categories', $d);
		$this->load->view('g/footer', $d);
		
	}
	
	public function addcat()
	{
	

		//define page title, custom js and active nav
		$d['title'] = 'Blog :: Add Category';
		$d['formtitle'] = 'Add new category';
		$d['js'] = 'news';
		$d['nav'] = 'news_categories';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('news/addcat', $d);
		$this->load->view('g/footer', $d);
		
	}
	
	public function editcat()
	{
		
		$query = $this->model->return_one_cat_by_id($this->uri->segment(3));
		if ($query->num_rows() == 0) { show_404(); }
		
		$row = $query->row();
		$d['row'] = $row; 	
		
				
		//define page title, custom js and active nav
		$d['title'] = 'Blog :: Categories :: Edit';
		$d['formtitle'] = 'Edit category';
		$d['js'] = 'news';
		$d['nav'] = 'news_categories';
		
		//load views
		$this->load->view('g/header', $d);
		$this->load->view('g/nav', $d);
		$this->load->view('news/editcat', $d);
		$this->load->view('g/footer', $d);
		
	}
	
	public function savecat()
	{
		
		// Save user
		$this->model->savecat();
		redirect($this->router->class.'/categories');
		
	}
	
	public function deletecat()
	{
		if($this->input->post('cat_id')) {
			$this->model->deletecat($this->input->post('cat_id'));
			$this->session->set_flashdata('msg', 'Category successfully deleted');
		} else {
			$this->session->set_flashdata('msg_err', '<strong>Whoops!</strong> Something went wrong. Please try again.');
		}
		
		redirect($this->router->class.'/categories');
		
	}
	
}

