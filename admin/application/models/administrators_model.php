<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 	
class Administrators_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = 'administrators';
    }
    
    public function save()
    {

		if($this->input->post('id')) {
			
			//update
			if($this->input->post('password')) {
			
				//update password
				$data = array(
					'password' => do_hash($this->input->post('password')),
					'updated' => date('Y-m-d H:i:s'),
					'updatedby' => $this->session->userdata('email')
				);
			
			} else {
				
				//update name/email
				$data = array(
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'email' => $this->input->post('email'),
					'updated' => date('Y-m-d H:i:s'),
					'updatedby' => $this->session->userdata('email')
				);
				
			}
		
			$this->db->update($this->table, $data, array('id' => $this->input->post('id')));
			$this->session->set_flashdata('msg', 'Your changes have been saved');
			
		} else {
		
			//add news
			$data = array(
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),
				'email' => $this->input->post('email'),
				'password' => do_hash($this->input->post('password')),
				'addedby' => $this->session->userdata('email'),
				'updated' => date('Y-m-d H:i:s'),
				'updatedby' => $this->session->userdata('email')
			);
			
			$this->db->insert($this->table, $data);
			$this->session->set_flashdata('msg', 'Administrator successfully added');
			
		}
		 
	    
    }
    
        
    
}