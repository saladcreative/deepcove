<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 	
class Optiswiss_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = 'info';
    }
    
    function return_all()
	{
		$this->db->order_by('id');
		return $this->db->get($this->table);
	}

    function return_one_by_id($id)
    {
    
        $this->db->where('id', $id);
        return $this->db->get($this->table);
    }

   function get_blocks($block_id)
    {
        $this->db->select('*');
        $this->db->where('info_page_id', $block_id);
        return $this->db->get('info_blocks');
    }

    function get_info_blocks($info_id)
    {
        $this->db->select('info_blocks.blocktitle, info_blocks.blockbody, info_blocks.blockimage, info_blocks.link2text, info_blocks.link2url, info_blocks.link3text, info_blocks.link4text, info_blocks.link5text, info_blocks.link3url, info_blocks.link4url, info_blocks.link5url, info_blocks.id');
        $this->db->from('info_blocks');
        $this->db->join('info', 'info.id = info_blocks.info_page_id');
        $this->db->where('info_blocks.id', $info_id);
        return $this->db->get();
    }

        
    function save()
    {
        
        
        //reformat date
        $tempdate = str_replace('/', '-', $this->input->post('date'));
        $date = date('Y-m-d', strtotime($tempdate));
    
        
        if($this->input->post('id')) {
        
            $id = $this->input->post('id');

            //update data
            $data = array(
                'title' => $this->input->post('title'),
                'metadesc' => $this->input->post('metadesc')

            );
                
            $this->db->update($this->table, $data, array('id' => $id));
            //update data

            $this->session->set_flashdata('msg', 'Your changes have been saved');

        } 

        
    }

    function save_row()
    { 
               
        $this->table = 'info_blocks';
        //reformat date
        $tempdate = str_replace('/', '-', $this->input->post('date'));
        $date = date('Y-m-d', strtotime($tempdate));
           
        if($this->input->post('id')) {
        
            $id = $this->input->post('id');

            //update data
            $data = array(
                'blocktitle' => $this->input->post('blocktitle'),
                'blockbody' => $this->input->post('blockbody'),
                'link2text' => $this->input->post('link2text'),
                'link2url' => $this->input->post('link2url'),
                'link3text' => $this->input->post('link3text'),
                'link3url' => $this->input->post('link3url'),
                'link4text' => $this->input->post('link4text'),
                'link4url' => $this->input->post('link4url'),
                'link5text' => $this->input->post('link5text'),
                'link5url' => $this->input->post('link5url')

            );
                
            $this->db->update($this->table, $data, array('id' => $id));
            //update data

            $this->session->set_flashdata('msg', 'Your changes have been saved');
     
        } 

        //upload image (article id)
        $imageresult = $this->model->uploadimg($id, '', '', 'blockimage');
        if($imageresult != 1) {
            if($imageresult != 'You did not select a file to upload.') {
                $this->db->update($this->table, array('publish' => 0), array('id' => $id));
                $this->session->set_flashdata('msg_err', $imageresult);
            }
        }
        //upload image

}
}
    