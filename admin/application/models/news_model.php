<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 	
class News_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = 'news';
    }
    
    function return_all_news()
	{
	
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('news_categories', 'news_categories.cat_id = '.$this->table.'.category');
		$this->db->order_by('date', 'desc');
		return $this->db->get();
	}
	
	function delete($id)
	{
		
		$this->db->where('id', $id);		
		$this->db->delete($this->table);
		
		$this->db->where('news_id', $id);		
		$this->db->delete('news_tags');
		
	}
	
	function deletecat($id)
	{
		
		//remove category
		$this->db->where('cat_id', $id);		
		$this->db->delete('news_categories');
		
		
	}
	
	function get_all_news_categories()
	{
		
		$this->db->order_by('cat_title', 'asc');		
		return $this->db->get('news_categories');
		
	}
	
	function no_articles_in_cat($i)
	{
		
		$this->db->where('category', $i);
		$q = $this->db->get($this->table);
		return $q->num_rows();
		
		
	}
	
	function get_tags($id)
	{
		
		$this->db->where('news_id', $id);
		$q = $this->db->get('news_tags');
		
		$datatag = '';
		foreach ($q->result() as $row) {
			$datatag .= $row->tag.',';
		}
		
		return $datatag;
		
	}
    
    function save()
    {
		
		//reformat date
		$tempdate = str_replace('/', '-', $this->input->post('date'));
		$date = date('Y-m-d', strtotime($tempdate));
		//reformat date		
		
		
		if($this->input->post('id')) {
			
			$id = $this->input->post('id');
			
			//update data
			$data = array(
				'title' => $this->input->post('title'),
				'date' => $date,
				'text' => $this->input->post('text'),
				'category' => $this->input->post('category'),
				'publish' => $this->input->post('publish'),
			
				'updated' => date('Y-m-d H:i:s'),
				'updatedby' => $this->session->userdata('email')
			);
				
			$this->db->update($this->table, $data, array('id' => $id));
			//update data
			
			//tags			
			$this->db->where('news_id', $id);
			$this->db->delete('news_tags'); 
			
			$this->db->where('news_id', $id);
			$this->db->delete('news_tags'); 
			//----
			$tagarray = str_getcsv($this->input->post('tags'));
			foreach($tagarray as $tag){
				$data = array(
					'tag' => $tag,
					'news_id' => $id,
					'tag_url' => url_title($tag, 'underscore', TRUE)
				);
				if($tag != '') {
					$this->db->insert('news_tags', $data);
				}
			}
			//tags
			
			$this->session->set_flashdata('msg', 'Your changes have been saved');
			
		} else {
		
			//sort out slug
			$slug = $this->model->slugchecker($this->input->post('slug'));
			//sort out slug
			
		
			//add news
			$data = array(
				'title' => $this->input->post('title'),
				'slug' => $slug,
				'date' => $date,
				'text' => $this->input->post('text'),
				'category' => $this->input->post('category'),
				'publish' => $this->input->post('publish'),
				
				'addedby' => $this->session->userdata('email'),
				'updated' => date('Y-m-d H:i:s'),
				'updatedby' => $this->session->userdata('email')
			);
			
			$this->db->insert($this->table, $data);
			$id = $this->db->insert_id();
			
			//tags
			$this->db->where('news_id', $id);
			$this->db->delete('news_tags'); 
			//----
			$tagarray = str_getcsv($this->input->post('tags'));
			foreach($tagarray as $tag){
				$data = array(
					'tag' => $tag,
					'news_id' => $id,
					'tag_url' => url_title($tag, 'underscore', TRUE)
				);
				if($tag != '') {
					$this->db->insert('news_tags', $data);
				}
			}
			//tags
			
			$this->session->set_flashdata('msg', 'Article successfully added');
			
		}
		
		//upload image (article id)
		$imageresult = $this->model->uploadimg($id);
		if($imageresult != 1) {
	 		if($imageresult != 'You did not select a file to upload.') {
				$this->session->set_flashdata('msg_err', $imageresult);
			}
		}
		//upload image
	    
    }
    
    function savecat()
    {
		
		if($this->input->post('cat_id')) {
			$this->db->update('news_categories', array('cat_title' => $this->input->post('cat_title')), array('cat_id' => $this->input->post('cat_id')));						
			$this->session->set_flashdata('msg', 'Your changes have been saved');
		} else {
			$this->db->insert('news_categories', array('cat_title' => $this->input->post('cat_title')));
			$this->session->set_flashdata('msg', 'Category successfully added');
		}

    }
    
    function return_one_cat_by_id($i)
    {
	    
	    $this->db->where('cat_id', $i);
	    return $this->db->get('news_categories');
	    
    }
    
    
        
    
}