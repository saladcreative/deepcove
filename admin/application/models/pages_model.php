<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 	
class Pages_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = 'pages';
    }
    
    function return_all()
	{
		return $this->db->get($this->table);
	}
	
	function return_one_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get($this->table);
	}
    
    function save()
    {
		if($this->input->post('id')) {
			
			$id = $this->input->post('id');
			
			if($this->input->post('seoonly') == 1) {

				$data = array(
					'pagetitle' => $this->input->post('pagetitle'),
					'metadesc' => $this->input->post('metadesc'),
					'updated' => date('Y-m-d H:i:s'),
					'updatedby' => $this->session->userdata('email')
				);
								
			} else {
			
				$data = array(
					'text' => $this->input->post('text'),
					'pagetitle' => $this->input->post('pagetitle'),
					'metadesc' => $this->input->post('metadesc'),
					'updated' => date('Y-m-d H:i:s'),
					'updatedby' => $this->session->userdata('email')
				);
							
			}

			$this->db->update($this->table, $data, array('id' => $id));
			$this->session->set_flashdata('msg', 'Your changes have been saved');
			
		}
    }
	
}