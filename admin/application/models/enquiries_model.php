<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 	
class Enquiries_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = 'enquiries';
    }
    
    function return_all()
	{
		return $this->db->get($this->table);
	}

    function return_one_by_id($id)
    {
    
        $this->db->where('id', $id);
        return $this->db->get($this->table);
    }

}