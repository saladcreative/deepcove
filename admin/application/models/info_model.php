<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 	
class Info_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = 'text';
    }
    
    function return_all()
	{
		$this->db->order_by('id');
		return $this->db->get($this->table);
	}

    function return_one_by_id($id)
    {
    
        $this->db->where('id', $id);
        return $this->db->get($this->table);
    }
        
    function save()
    {
        
        
        //reformat date
        $tempdate = str_replace('/', '-', $this->input->post('date'));
        $date = date('Y-m-d', strtotime($tempdate));
    
        
        if($this->input->post('id')) {
        
            $id = $this->input->post('id');

            //update data
            $data = array(
                'title' => $this->input->post('title'),
                'image' => $this->input->post('image'),
                'firstheading' => $this->input->post('firstheading'),
                'firstsection' => $this->input->post('firstsection'),
                'secondheading' => $this->input->post('secondheading'),
                'secondsection' => $this->input->post('secondsection'),
                'metadesc' => $this->input->post('metadesc'),
                'seotitle' => $this->input->post('seotitle')

            );
                
            $this->db->update($this->table, $data, array('id' => $id));
            //update data

            $this->session->set_flashdata('msg', 'Your changes have been saved');

            
        } 

        
    }

}
    