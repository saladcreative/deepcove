<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 	
class Home_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = 'home';
    }
    
    function return_all()
	{
		$this->db->order_by('id');
		return $this->db->get($this->table);
	}

    function return_one_by_id($id)
    {
    
        $this->db->where('id', $id);
        return $this->db->get($this->table);
    }

        
    function save()
    {
        
        
        //reformat date
        $tempdate = str_replace('/', '-', $this->input->post('date'));
        $date = date('Y-m-d', strtotime($tempdate));
    
        
        if($this->input->post('id')) {
        
            $id = $this->input->post('id');

            //update data
            $data = array(
                'heading' => $this->input->post('heading'),
                'body' => $this->input->post('body'),
                'buttontitle' => $this->input->post('buttontitle'),
                'boxonetitle' => $this->input->post('boxonetitle'),
                'boxonebody' => $this->input->post('boxonebody'),
                'boxtwotitle' => $this->input->post('boxtwotitle'),
                'boxtwobody' => $this->input->post('boxtwobody'),
                'boxthreetitle' => $this->input->post('boxthreetitle'),            
                'boxthreebody' => $this->input->post('boxthreebody'),
                
                'slide1title' => $this->input->post('slide1title'),
                'slide1text' => $this->input->post('slide1text'),
                'slide1image' => $this->input->post('slide1image'),

                'slide2title' => $this->input->post('slide2title'),
                'slide2text' => $this->input->post('slide2text'),
                'slide2image' => $this->input->post('slide2image'),

                'slide3title' => $this->input->post('slide3title'),
                'slide3text' => $this->input->post('slide3text'),
                'slide3image' => $this->input->post('slide3image'),

                'slide4title' => $this->input->post('slide4title'),
                'slide4text' => $this->input->post('slide4text'),
                'slide4image' => $this->input->post('slide4image'),

                'slide5title' => $this->input->post('slide5title'),
                'slide5text' => $this->input->post('slide5text'),
                'slide5image' => $this->input->post('slide5image'),
            );
                
            $this->db->update($this->table, $data, array('id' => $id));
            //update data

            $this->session->set_flashdata('msg', 'Your changes have been saved');

        } 

        
    }
}
    