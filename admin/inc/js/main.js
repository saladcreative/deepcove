$(document).ready(function() {

	$('.validateform').each(function () {
	    $(this).validate();
	});
	
	$("#submit").click(function() {
	    if ($(".validateform").valid()) {
	        $(this).addClass('disabled');
	        $('#cancelbutton').addClass('disabled');
	        $(this).prop('value', 'Processing...');
	        $('#processing').show();
	    }
	});
	
	// admininstrators form validation
	$(".validatepw").validate({
		rules: {
			firstname: "required",
			lastname: "required",
			email: {
				required: true,
				email: true
			},
			password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			}
		},
		messages: {
			firstname: "Please enter your first name",
			lastname: "Please enter your last name",
			email: "Please enter a valid email address",
			password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			confirm_password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo: "Please enter the same password as above"
			}
		}
	});
	
	$("#titlefield").keyup(function() {
		$('#slug').html($(this).val().toLowerCase().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-'));
		$('input[name="slug"]').val($(this).val().toLowerCase().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-'));
	});
	

	
	$( ".datepicker" ).datepicker({ 
		dateFormat: 'dd/mm/yy',
		showAnim: 'slideDown'
	});
	
		
	/* sidebar open */
	var seachSpeed = 0.2;
	var searchBox = $(".right-bar.search-box");
	var rightOpen = $('.right-open');
	var rightBar = $('.right-bar');
	var rightShow;
	var rightHide;
	
	function rightPosition() {
		if($(window).width() <= 950) {
			rightShow = 46;
			rightHide = -215;
		}else{
			rightShow = 221;
			rightHide = -40;
		}
	}
	rightPosition();
	$(window).resize(rightPosition);
	
	
	$('.container').click(function() {
		TweenLite.to($('.right-bar.open'), seachSpeed, {left:rightHide, ease:Power1.easeOut});
		$('.right-bar.open').removeClass('open');
		$('.right-open.toggle').removeClass('toggle');
	});
	
	function elementAction(element) {
		
		if(rightBar.hasClass('open')) {
			
			
			if(!element.hasClass('open')) {
				TweenLite.to(element, seachSpeed, {delay: seachSpeed, left:rightShow, ease:Power1.easeOut});
				setTimeout(function() {
					element.addClass('open');
				}, seachSpeed );
			}
			
			TweenLite.to($('.right-bar.open'), seachSpeed, {left:rightHide, ease:Power1.easeOut});
			$('.right-bar.open').removeClass('open');
			
		}else{
			
			TweenLite.to(element, seachSpeed, {left:rightShow, ease:Power1.easeOut});
			element.addClass('open');
			
		}
	}
	
	rightOpen.click(function() {
		if(!$(this).hasClass('toggle')) {
			$('.right-open.toggle').removeClass('toggle');
			$(this).addClass('toggle');
		}else{
			$(this).removeClass('toggle');
		}
		var eleHref = $(this).data('href');
		elementAction($(eleHref));
	});
	
	
	
	
	// navigation
	$('#navigation li a').click(function() {
		var $this = $(this);
		var subMenu = $(this).closest('li').find('ol');
		if(subMenu.is(':hidden')) {
			$('#navigation ol').slideUp(200);
			$('#navigation a.active').removeClass('active');
			$this.addClass('active');
			subMenu.stop(true,true).slideDown(200,function() {
				$(".sidebar-nav").mCustomScrollbar("update");
			});
		}else{
			$this.removeClass('active');
			subMenu.stop(true,true).slideUp(200, function() {
				$(".sidebar-nav").mCustomScrollbar("update");
			});
		}
	});
	
	
	// responsive layout
	// navbar menu
	$('.menu-holder').click(function() {
		if($('.menu').is(':hidden')) {
			$('.menu').css('display','inline-block');
		}else{
			$('.menu').css('display','none');
		}
	});
	// sidebar menu
	$('.sidebar-nav').on('click', '#navigation', function(e) {
		if($(this).css('position') === 'static') {
			var eleToggle = $('#navigation ul');
			if(eleToggle.is(':hidden')) {
				eleToggle.show(0);
			}else{
				if(e.clientY <= 99) {
					eleToggle.hide(0);
				}
			}
		}
	});
	
	
	prettyPrint();
	
	/* drop down */
	$('.drop-down span').click(function() {
		if($(this).next('ul').is(':hidden')) {
			$(this).next('ul').slideDown(150);
		}else{
			$(this).next('ul').slideUp(150);
		}
		$('.drop-down ul').not($(this).closest('.drop-down').find('ul')).hide();
	});
	$('.drop-down ul a').click(function() {
		var selectValue = $(this).html();
		$(this).closest('ul').hide().prev('span').html(selectValue);
	});

	
	
	
	
	/* checkbox styling */
	$('input[type="checkbox"]:not(.simple)').each(function() {
		
		$(this).hide().wrap('<span class="check-wrap"></span>').after('<a href="javascript:;"></a>');
		var $checkbox = $(this).parent('span').find('a');
		
		
		if($(this).is(':checked') && $(this).is(':disabled')){
			$checkbox.addClass('disabled-checked');
		}else if($(this).is(':disabled')){
			$checkbox.addClass('disabled');
		}else if($(this).is(':checked')){
			$checkbox.addClass('checked');
		}else if($(this).is(':disabled')){
			$checkbox.addClass('disabled');
		}
		
		if($(this).is(':disabled')){
			$checkbox.addClass('disabled');
		}
	});
	
	$('span.check-wrap').on('click', 'a', function() {
		var $input = $(this).prev('input');
		if(!$input.is(':disabled') && !$(this).hasClass('disabled-checked')){
			if(!$(this).hasClass('checked')){
				$(this).addClass('checked');
				$input.attr('checked', 'checked');
			}else{
				$(this).removeClass('checked');
				$input.removeAttr('checked');
			}
		}
	});
	
	
	
	/* placeholder */
	$('[placeholder]').focus(function() {
		var input = $(this);
		if (input.val() === input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		}
	}).blur(function() {
		var input = $(this);
		if (input.val() === '' || input.val() === input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		}
	}).blur();
	$('[placeholder]').parents('form').submit(function() {
		$(this).find('[placeholder]').each(function() {
			var input = $(this);
			if (input.val() === input.attr('placeholder')) {
				input.val('');
			}
		});
	});
	
	
	
	// table sorter init
	$(".tblr").addClass("beautifulData").beautify({
		pageSize : 7,
		pagerSize : 5
	});
	
	/* Enable table search */
	$(".table-search").keyup(function() {
		$(this).closest('div').next().beautify("rebuild", { globalFilter : $(".table-search").val() });
	}); 
	
	$('.tip').tooltip();
	
	$('.redactor').redactor({ 
		imageUpload: '../../fileuploads/do_upload_img/',
		fileUpload: '../../fileuploads/do_upload_file/',
		minHeight: 200,
		convertLinks: true,
		buttons: ['html', '|', 'formatting', '|', 'alignment', 'bold', 'italic', 'deleted', '|', 'unorderedlist', 'orderedlist', 'outdent', 'indent', '|', 'image', 'video', 'file', 'link']
	});
	
	$('#tags').tagsInput({width:'100%',height:'150px'});
	
	/* styling dropdown */
	$('select.styled').customSelect();
	
	$('#fileupload').change(function(){
	    var filename = $(this).val().replace(/C:\\fakepath\\/i, '')
	    if(filename != '') {
		    $('#filepath').show().html('<i class="icon-file"></i> '+filename);
	    } else {
		    $('#filepath').hide();
	    }
	});


});