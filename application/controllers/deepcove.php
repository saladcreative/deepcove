<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deepcove extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->cookies();
    } 

	public function index()
	{
		$query = $this->main_model->get_page_by_slug($this->uri->segment(1));
		$row = $query->row();
		$d['row'] = $row;

		if($query->num_rows() == 0) {
			redirect('404');
		};

		$d['l'] = $this->main_model->get_deepcove_contents();  

		$d['bannerimg'] = $row->img;
		$d['ogimage'] = $row->img;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('deepcove/index', $d);
		$this->load->view('g/footer', $d);
	}

	public function landing_page()
	{
		$query = $this->main_model->get_page_by_slug($this->uri->segment(1)."/".$this->uri->segment(2)); 
		$row = $query->row();
		$d['row'] = $row;

		if($query->num_rows() == 0) {
			redirect('404');
		};

		$d['infotext'] = $this->main_model->get_info_blocks($row->id);

		$d['bannerimg'] = $row->img;
		$d['ogimage'] = $row->img;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('deepcove/landing_page', $d);
		$this->load->view('g/footer', $d);
	}

	public function view_article()
	{
		$query = $this->main_model->get_page_by_slug($this->uri->segment(1)."/".$this->uri->segment(2)."/".$this->uri->segment(3));
		$row = $query->row();
		$d['row'] = $row;

		if($query->num_rows() == 0) {
			redirect('404');
		};
		
		$d['t'] = $this->main_model->get_text($row->id);  

		$d['bannerimg'] = $row->img;
		$d['ogimage'] = $row->img;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('deepcove/view', $d);
		$this->load->view('g/footer', $d);
	}

}