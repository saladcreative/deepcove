<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom404 extends MY_Controller {

	
	public function index()
	{
		
		$pageid = 48;

		$query = $this->main_model->get_page($pageid);
		$row = $query->row();
		$d['row'] = $row;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('404', $d);
		$this->load->view('g/footer', $d);
	}
	
}

