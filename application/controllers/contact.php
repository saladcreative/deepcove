<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->cookies();

    } 

	public function index()
	{

		$pageid = 8;

		$query = $this->main_model->get_page($pageid);
		$row = $query->row();
		$d['row'] = $row;

		$d['bannerimg'] = $row->img;
		$d['ogimage'] = $row->img;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('contact/contact', $d);
		$this->load->view('g/footer', $d);
	}

	public function thankyou()
	{

		$pageid = 21;

		$query = $this->main_model->get_page($pageid);
		$row = $query->row();
		$d['row'] = $row;

		$d['bannerimg'] = $row->img;
		$d['ogimage'] = $row->img;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('contact/thank-you', $d);
		$this->load->view('g/footer', $d);
	}

	public function submitform()
	{
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

		if($this->input->post('email')) {

			// Catch the user's answer
			$captcha_answer = $this->input->post('g-recaptcha-response');

			// Verify user's answer
			$response = $this->recaptcha->verifyResponse($captcha_answer);

			// Processing ...
			if ($response['success']) {

				if($this->input->post('name') == '') {
					redirect();
				}
				if($this->input->post('email') == '') {
					redirect();
				}
				if($this->input->post('telephonenum') == '') {
					redirect();
				}
				if($this->input->post('company') == '') {
					redirect();
				}
				if($this->input->post('comment') == '') {
					redirect();
				}

				$blacklist = array('detectify', 'passwd', '()', '.txt', '\etc', '/etc', 'c:', 'netstat', 'pentest', '[removed]', 'submitform', 'file:', '/.', '/..', '/...');

				foreach ($blacklist as $spamitem) {
					if(strpos($this->input->post('name'), $spamitem) !== false) {
						redirect();
					}
					if(strpos($this->input->post('email'), $spamitem) !== false) {
						redirect();
					}
					if(strpos($this->input->post('telephonenum'), $spamitem) !== false) {
						redirect();
					}
					if(strpos($this->input->post('company'), $spamitem) !== false) {
						redirect();
					}
					if(strpos($this->input->post('comment'), $spamitem) !== false) {
						redirect();
					}
				}

				$requiredlist = array('@');

				foreach ($requiredlist as $requireditem) {
					if(strpos($this->input->post('email'), $requireditem) === false) {
						redirect();
					}
				}

			} else {
				$this->session->set_flashdata('error', 1);
				$this->session->set_flashdata('name', $this->input->post('name'));
				$this->session->set_flashdata('email', $this->input->post('email'));
				$this->session->set_flashdata('telephonenum', $this->input->post('telephonenum'));
				$this->session->set_flashdata('company', $this->input->post('company'));
				$this->session->set_flashdata('comment', $this->input->post('comment'));
				redirect('contact#formanchor');

			} // end recaptcha if statement

			//honey in the honeypot Pooh?
			if($this->input->post('alternate') != '') {

				redirect();

			} else {

				// Processing ...
				$data = array(
					'name' => $this->input->post('name'),
					'email' => $this->input->post('email'),
					'telephonenum' => $this->input->post('telephonenum'),
					'company' => $this->input->post('company'),
					'comment' => $this->input->post('comment')
				);

				//-----------send email
				$this->load->library('email');

				$this->email->from('info@deepcove.co.uk', 'Deepcove EU');
				$this->email->to('info@deepcove.co.uk');
				//$this->email->to('adam@saladcreative.com'); 
				$this->email->subject('Deepcove EU Website Enquiry');
				
				$msg = $this->load->view('contact/contact_email', $data, true);
				$this->email->message($msg);	
				
				$this->email->send();
				//-----------send email

				$this->main_model->contact_insert($data);

				redirect('thank-you');
 
			}
		}
	}

}