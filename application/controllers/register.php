<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->cookies();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation'));
    } 

	public function index()
	{

		$this->load->model('register_model');  

		$pageid = 6;

		$query = $this->main_model->get_page($pageid);
		$row = $query->row();
		$d['row'] = $row;

		$d['r'] = $this->register_model->get_contents();  

		$d['bannerimg'] = $row->img;
		$d['ogimage'] = $row->img;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('account/registration', $d);
		$this->load->view('g/footer', $d);
	}

	public function thankyou()
	{

		$pageid = 47;

		$query = $this->main_model->get_page($pageid);
		$row = $query->row();
		$d['row'] = $row;

		$d['bannerimg'] = $row->img;
		$d['ogimage'] = $row->img;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('account/thank-you', $d);
		$this->load->view('g/footer', $d);
	}

	public function createAccount()
	{

			// Processing ...
			if($this->input->post('emailmain')) {

				if($this->input->post('alternate') != '') {

					redirect();

				} else {

					$data = array(
						'companyname' => $this->input->post('companyname'),
						'country' => $this->input->post('country'),
						'title' => $this->input->post('title'),
						'firstname' => $this->input->post('firstname'),
						'lastname' => $this->input->post('lastname'),
						'street' => $this->input->post('street'),
						'postcode' => $this->input->post('postcode'),
						'city' => $this->input->post('city'),
						'country' => $this->input->post('country'),
						'telephone' => $this->input->post('telephone'),
						'emailmain' => $this->input->post('emailmain'),
						'emailaccount' => $this->input->post('emailaccounting'),
						'vat' => $this->input->post('vat'),
						'comment' => $this->input->post('comment'),
						'optiswiss' => $this->input->post('optiswiss'),
						'deepcove' => $this->input->post('deepcove'),
						'date' => date('d-m-y')
					);

					//-----------send email
					$this->load->library('email');
					
					// $this->email->initialize($config);

					$this->email->from('info@deepcove.eu', 'Deepcove EU');
					$this->email->to('info@deepcove.eu'); 
					$this->email->bcc('brandon@saladcreative.com'); 
					$this->email->subject('Deepcove EU Account Registration');
					
					$msg = $this->load->view('account/register_email', $data, true);
					$this->email->message($msg);	
					
					$this->email->send();
					//-----------send email

					$this->main_model->form_insert($data);

					redirect('account/thank-you');

				}

			}  else {
		    // Something goes wrong
		    redirect();
		}

	}

}