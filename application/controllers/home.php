<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->cookies();
    } 

	public function index()
	{
		$pageid = 1;

		$query = $this->main_model->get_page($pageid);
		$row = $query->row();
		$d['row'] = $row;

		$d['h'] = $this->main_model->get_home_contents();  

		$d['bannerimg'] = $row->img;
		$d['ogimage'] = $row->img;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('home', $d);
		$this->load->view('g/footer', $d);
	}

	public function template_page()
	{
		$query = $this->main_model->get_page_by_slug($this->uri->segment(1));
		$row = $query->row();
		$d['row'] = $row;

		if($query->num_rows() == 0) {
			redirect('404');
		};

		$d['t'] = $this->main_model->get_text($row->id);  

		$d['bannerimg'] = $row->img;
		$d['ogimage'] = $row->img;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('template', $d);
		$this->load->view('g/footer', $d);

	}

	public function iframe()
	{
		$pageid = 5;

		$query = $this->main_model->get_page($pageid);
		$row = $query->row();
		$d['row'] = $row;

		$d['bannerimg'] = $row->img;
		$d['ogimage'] = $row->img;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('deepcove/iframe', $d);
		$this->load->view('g/footer', $d);
	}

}

