<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brands extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->cookies();
    } 

	public function index()
	{
		$pageid = 7;

		$query = $this->main_model->get_page($pageid);
		$row = $query->row();
		$d['row'] = $row;

		$d['bannerimg'] = $row->img;
		$d['ogimage'] = $row->img;

		$d['pagetitle'] = $row->pagetitle;
		$d['metadesc'] = $row->metadesc;

		$this->load->view('g/header', $d);
		$this->load->view('brands', $d);
		$this->load->view('g/footer', $d);
	}
	
}