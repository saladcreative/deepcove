<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function cookies()
    {
		if(!isset($_COOKIE['cookieconsent'])) {
			setcookie("cookieconsent", 1, time()+60*60*24*30); // valid for 1 month
			$_COOKIE['cookieconsent'] = 1;
		}
    }
}