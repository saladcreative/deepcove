<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions {

    public function show_404($page = '', $log_error = TRUE)
    {
		include APPPATH . 'config/routes.php';
		
		// By default we log this, but allow a dev to skip it
		if ($log_error) {
			log_message('error', '404 Page Not Found --> '.$page);
		}
		
		if(!empty($route['404_override']) ){
			
			
			$d['title'] = '404 - Page not found';
			$d['nav'] = '';
			$d['bg'] = 'home.jpg';
			
			$CI =& get_instance();
			
			$CI->load->view('g/header', $d);
			$CI->load->view('404', $d);
			$CI->load->view('g/footer');
			
		     echo $CI->output->get_output();
		     exit;
		     
		} else {
		
			$heading = "404 Page Not Found";
			$message = "The page you requested was not found.";
			
			echo $this->show_error($heading, $message, 'error_404', 404);
			exit;
		}
    }

} 