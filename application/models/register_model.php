<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    } 
    
    function get_page($id)
    {
    	$this->db->where('id', $id);
    	return $this->db->get('pages');
    }

    function get_contents() {
        $query = $this->db->get('register');
        $row = $query->row();
        return $row;        
    }

}