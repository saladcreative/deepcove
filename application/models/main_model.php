<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    } 
    
    function get_page($id)
    {
    	$this->db->where('id', $id);
    	return $this->db->get('pages');
    }

    function get_page_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        return $this->db->get('pages');
    }

    function contact_insert($data){
        $this->db->insert('enquiries', $data);
    }

    function form_insert($data){
        $this->db->insert('contact_info', $data);
    }

    function get_info_blocks($id)
    {
        $this->db->where('pageid', $id);
        $this->db->where('publish', 1);
        return $this->db->get('info_blocks');
    }

    function get_text($id)
    {
        $this->db->where('pageid', $id);
        $query = $this->db->get('text');
        $row = $query->row();
        return $row;        
    }

    function get_home_contents() {
        $query = $this->db->get('home');
        $row = $query->row();
        return $row;        
    }

    function get_optiswiss_contents() {
        $query = $this->db->get('optiswiss_landing');
        $row = $query->row();
        return $row;        
    }

    function get_deepcove_contents() {
        $query = $this->db->get('deepcove_landing');
        $row = $query->row();
        return $row;        
    }

}