<div class="text">
	<div class="container small-container relative <?php if($this->uri->segment(1) === 'complete-spectacles') { echo 'complete-spectacles'; } ?>">
		<div class="row">
			<div class="column small-12">
				<div class="hv-center text-center">
					<?php echo $this->typography->format_characters($row->text);?>
				</div>
			</div>
		</div>
	</div>
	<div class="container light-grey-body text-body">
		<div class="container">
			<div class="row">
				<div class="column small-12 medium-8 medium-push-2 text-container">
				<?php if($t->firstheading) { echo htmlspecialchars_decode($t->firstheading); } ?>
					<?php if($t->firstsection) { echo htmlspecialchars_decode($t->firstsection); } ?>
					<?php if($t->secondheading) { echo htmlspecialchars_decode($t->secondheading); } ?>
					<?php if($t->secondsection) { echo htmlspecialchars_decode($t->secondsection); } ?>
					<?php if($t->thirdheading) { echo htmlspecialchars_decode($t->thirdheading); } ?>
					<?php if($t->thirdsection) { echo htmlspecialchars_decode($t->thirdsection); } ?>
					<?php if($t->fourthheading) { echo htmlspecialchars_decode($t->fourthheading); } ?>
					<?php if($t->fourthsection) { echo htmlspecialchars_decode($t->fourthsection); } ?>

					<?php if($this->uri->segment(1) === 'marketing') { ?>
						<p class="find-out enquire">
							<a href="<?php echo base_url() ?>contact" class="button primary">
								Enquire today
								<img src="<?= base_url() ?>/inc/img/next.svg" class="next">
							</a>
						</p>
					<?php } ?>

				</div>
			</div>
		</div>
	</div>
</div>