<div class="container grey-container landing">
	<div class="row main-container">
		<div class="column small-12 medium-8 medium-push-2 text-center">
			<h1 class="uppercase"><?php echo $this->typography->format_characters($l->title);?></h1>
		</div>
	</div>
	<div class="row absolute-row">
		<a href="<?php echo base_url()?>optiswiss/lenses">
			<div class="column small-12 medium-4">
				<img src="<?php echo base_url() ?>uploads/slir/w400-q80/<?=rawurlencode($l->boxoneimage)?>" alt="<?php echo $row->title?>" class="responsive-img">
				<div class="grey-box relative">
					<div class="v-center">
						<div class="text-container">
							<div class="very-small-border"></div>
							<h4><?php echo $this->typography->format_characters($l->boxtitleone);?></h4>
							<p class="find-out">
								<a href="<?php echo base_url()?>optiswiss/lenses">
									View the range
									<img src="<?= base_url() ?>/inc/img/black-next.svg" class="next">
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</a>
		<a href="<?php echo base_url()?>optiswiss/coatings">
			<div class="column small-12 medium-4">
				<img src="<?php echo base_url() ?>uploads/slir/w400-q80/<?php echo rawurlencode($l->boxtwoimage)?>" alt="<?php echo $row->title?>" class="responsive-img">
				<div class="grey-box relative">
					<div class="v-center">
						<div class="text-container">
							<div class="very-small-border"></div>
							<h4><?php echo $this->typography->format_characters($l->boxtitletwo);?></h4>
							<p class="find-out">
								<a href="<?php echo base_url()?>optiswiss/coatings">
									View the range
									<img src="<?= base_url() ?>/inc/img/black-next.svg" class="next">
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</a>
		<a href="<?php echo base_url()?>optiswiss/special-products">
			<div class="column small-12 medium-4">
				<img src="<?php echo base_url() ?>uploads/slir/w400-q80/<?php echo rawurlencode($l->boxthreeimage)?>" alt="<?php echo $row->title?>" class="responsive-img">
				<div class="grey-box relative">
					<div class="v-center">
						<div class="text-container">
							<div class="very-small-border"></div>
							<h4><?php echo $this->typography->format_characters($l->boxtitlethree);?></h4>
							<p class="find-out">
								<a href="<?php echo base_url()?>optiswiss/special-products">
									View the range
									<img src="<?php base_url() ?>/inc/img/black-next.svg" class="next">
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>