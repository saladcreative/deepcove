<div class="container iframe">
	<div class="row">
		<div class="main-container relative">
			<div class="column small-12 large-10 text-center">
				<div class="hv-center main-content">
					<h1 class="uppercase">Order your lenses</h1>
					<div class="content">
						<p>All our lenses can be ordered via our online ordering system below, just enter your login and password. Its simple and straightforward, you can even order with remote edging.</p>
						<p class="uppercase signup"><a href="/register"><span>Create account</span></a> to place an order</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row hidden-text">
		<div class="text-center">
			<p>To order lenses with Deepcove, please use a Desktop computer.</p>
		</div>
	</div>

	<div class="row">
		<div class="column small-12">
			<div class="iframe-container">
				<!-- <iframe src="https://www2.solenzara.co.uk/deepcovestart4.html"></iframe> -->
				<iframe src="https://www.solenzara.co.uk/deepcovestart.html"></iframe>	
			</div>
		</div>
	</div>
</div>
