<div class="container grey-container landing">
	<div class="row main-container">
		<div class="column small-12 medium-8 medium-push-2 text-center">
			<h1 class="uppercase"><?php echo $this->typography->format_characters($l->title);?></h1>
			<p><?php echo $this->typography->format_characters($l->body);?></p>
		</div>
	</div>
	<div class="row absolute-row">
		<a href="<?php echo base_url() ?>deepcove/lenses">
			<div class="column small-12 medium-4">
				<img src="<?php echo base_url(); ?>inc/img/deepcove-landing/lenses.jpg" alt="Deepcove" class="responsive-img">
				<div class="grey-box relative">
					<div class="v-center">
						<div class="text-container">
							<div class="very-small-border"></div>
							<h4><?php echo $this->typography->format_characters($l->boxtitleone);?></h4>
							<p class="find-out">
								<a href="<?php echo base_url() ?>deepcove/lenses">
									View Lenses
									<img src="<?= base_url() ?>/inc/img/black-next.svg" class="next">
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</a>
		<a href="<?php echo base_url() ?>deepcove/treatments">
			<div class="column small-12 medium-4">
				<img src="<?php echo base_url(); ?>inc/img/deepcove-landing/treatments.jpg" alt="Deepcove" class="responsive-img">
				<div class="grey-box relative">
					<div class="v-center">
						<div class="text-container">
							<div class="very-small-border"></div>
							<h4><?php echo $this->typography->format_characters($l->boxtitletwo);?></h4>
							<p class="find-out">
								<a href="<?php echo base_url() ?>deepcove/treatments">
									View Treatments
									<img src="<?= base_url() ?>/inc/img/black-next.svg" class="next">
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</a>
		<a href="<?php echo base_url() ?>deepcove/extraservices">
			<div class="column small-12 medium-4">
				<img src="<?php echo base_url(); ?>inc/img/deepcove-landing/services.jpg" alt="Deepcove" class="responsive-img">
				<div class="grey-box relative">
					<div class="v-center">
						<div class="text-container">
							<div class="very-small-border"></div>
							<h4><?php echo $this->typography->format_characters($l->boxtitlethree);?></h4>
							<p class="find-out">
								<a href="<?php echo base_url() ?>deepcove/extraservices">
									View Extra Services
									<img src="<?php base_url() ?>/inc/img/black-next.svg" class="next">
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>