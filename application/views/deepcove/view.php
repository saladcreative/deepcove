<div class="text">
	<div class="container small-container relative">
		<div class="row">
			<div class="column small-12">
				<div class="hv-center text-center">
					<h1><?php echo $this->typography->format_characters($t->title);?></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="container light-grey-body text-body">
		<div class="container">
			<div class="row">
				<div class="column small-12 medium-8 medium-push-2 text-container">
					<?php if($t->firstheading) { echo htmlspecialchars_decode($t->firstheading); } ?>
					<?php if($t->firstsection) { echo htmlspecialchars_decode($t->firstsection); } ?>
					<?php if($t->secondheading) { echo htmlspecialchars_decode($t->secondheading); } ?>
					<?php if($t->secondsection) { echo htmlspecialchars_decode($t->secondsection); } ?>
					<?php if($t->thirdheading) { echo htmlspecialchars_decode($t->thirdheading); } ?>
					<?php if($t->thirdsection) { echo htmlspecialchars_decode($t->thirdsection); } ?>
					<?php if($t->fourthheading) { echo htmlspecialchars_decode($t->fourthheading); } ?>
					<?php if($t->fourthsection) { echo htmlspecialchars_decode($t->fourthsection); } ?>
					<a class="button primary back-button" onclick="window.history.go(-1)">
						<img src="<?php echo base_url() ?>inc/img/black-next.svg" class="back">
						Back
					</a>
				</div>
			</div>
		</div>
	</div>
</div>