<div class="container info relative">
	<div class="dark-grey-container" style="background-image:url(<?php echo base_url('inc/img').'/'.rawurlencode($row->img); ?>);">
		<div class="row">
			<div class="column small-12">
				<div class="hv-center">
					<h1 class="uppercase"><?php echo $this->typography->format_characters($row->title);?></h1>
					<p><?php echo $this->typography->format_characters($row->subtitle);?></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="info-copy">

 <?php $i=1; foreach($infotext->result() as $blockrow) { ?>

	<?php if(($i+2) % 3 == 0) { ?>
 	<div class="container white-background">
		<div class="row relative block-one">
			<div class="column small-12 medium-6">
				<div class="featured-image">
					<img src="<?php echo base_url() ?>uploads/slir/w600-q80/<?php echo rawurlencode($blockrow->blockimage)?>" alt="<?php echo $row->title?>" class="responsive-img">
				</div>
			</div>
			<div class="column small-12 medium-6">
				<h2 class="uppercase"><?php echo $this->typography->format_characters($blockrow->blocktitle);?></h2>
				<div class="body">
					<?php echo $blockrow->blockbody; ?>
				</div>
				<?php if($blockrow->has_link) { ?>
					<p class="find-out">
						<a href="<?php echo base_url().$blockrow->link; ?>">
							Find out more
							<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
						</a>
					</p>
				<?php } ?>
			</div>
		</div>
	</div>           
    <?php } ?>

	<?php if(($i+1) % 3 == 0) { ?>
 	<div class="container grey-background">
		<div class="row relative block-two">
			<div class="column small-12 medium-6">
				<div class="featured-image">
					<img src="<?php echo base_url() ?>uploads/slir/w600-q80/<?php echo rawurlencode($blockrow->blockimage)?>" alt="<?php echo $row->title?>" class="responsive-img">
				</div>
			</div>
			<div class="column small-12 medium-6">
				<h2 class="uppercase"><?php echo $this->typography->format_characters($blockrow->blocktitle);?></h2>
				<div class="body">
					<?php echo $blockrow->blockbody; ?>
				</div>
				<?php if($blockrow->has_link) { ?>
					<p class="find-out">
						<a href="<?php echo base_url().$blockrow->link; ?>">
							Find out more
							<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
						</a>
					</p>
				<?php } ?>
			</div>
		</div>
	</div>           
    <?php } ?>

	<?php if($i % 3 == 0) { ?>
	<div class="container half-background relative">
		<div class="fourty-percent"></div>
		<div class="sixty-percent"></div>
		<div class="row relative block-three">
			<div class="column small-12 medium-6">
				<div class="featured-image">
					<img src="<?php echo base_url() ?>uploads/slir/w600-q80/<?php echo rawurlencode($blockrow->blockimage)?>" alt="<?php echo $row->title?>" class="responsive-img">
				</div>
			</div>
			<div class="column small-12 medium-6">
				<h2 class="uppercase"><?php echo $this->typography->format_characters($blockrow->blocktitle);?></h2>
				<div class="body">
					<?php echo $blockrow->blockbody; ?>
				</div>
				<?php if($blockrow->has_link) { ?>
					<p class="find-out">
						<a href="<?php echo base_url().$blockrow->link; ?>">
							Find out more
							<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
						</a>
					</p>
				<?php } ?>
			</div>
		</div>
	</div>           
    <?php } ?>

<?php $i++; } ?>

	<div class="container half-bg-container">
		<div class="row enquire-block">
			<div class="column small-12 text-center">
				<p class="find-out">
					<a href="<?php echo base_url() ?>contact" class="button primary">
						Enquire today
						<img src="<?php echo base_url() ?>/inc/img/next.svg" class="next">
					</a>
				</p>
			</div>
		</div>
	</div>
</div>
