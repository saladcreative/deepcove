<footer>
	<div class="container footer-container">
		<div class="row relative">
			<div class="small-12 medium-6 large-4 column">
				<div class="footer-links uppercase">
					<ul>
						<li><a href="<?php echo base_url()?>optiswiss">Optiswiss</a></li>
						<li><a href="<?php echo base_url()?>deepcove">Deepcove RX</a></li>
						<li><a href="<?php echo base_url()?>marketing">Marketing Services</a></li>
						<li><a href="<?php echo base_url()?>terms">Terms &amp; Conditions</a></li>
						<li><a href="<?php echo base_url()?>privacy">Privacy Policy</a></li>
						<li><a href="<?php echo base_url()?>about">About</a></li>
						<li><a href="<?php echo base_url()?>contact">Contact</a></li>
					</ul>
				</div>
			</div>
			<div class="small-12 medium-6 large-4 column">
				<div class="footer-text">
					<p class="uppercase">
						Unit 9 Rockhaven Business Centre <br>
						Longhedge <br>
						Salisbury <br >
						SP4 6RT <br>
						Wiltshire <br>
						United Kingdom
					</p>
					<p class="uppercase">
						<a href="tel:+441985800425">+44 (0) 1985 800 425</a><br />
						<a href="mailto:info@deepcove.co.uk">info@deepcove.co.uk</a>
					</p>
				</div>
			</div>
			<div class="small-12 medium-12 large-4 column">
				<div class="footer-logo">
					<img src="<?php echo base_url()?>inc/img/white-logo.svg" alt="Deepcove EU" />
					<p class="uppercase">&copy; Copyright <?php echo date("Y") ?></p>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="column small-12">
				<div class="footer-strip">
					<p class="uppercase">Site by <a href="https://saladcreative.com" target="_blank">Salad</a></p>
				</div>
			</div>
		</div>
	</div>
</footer>


	<?php if(isset($_COOKIE['cookieconsent']) && $_COOKIE['cookieconsent'] == 1)  { ?>
	<script>
		function create_cookie(name, value, days2expire, path) {
			var date = new Date();
			date.setTime(date.getTime() + (days2expire * 24 * 60 * 60 * 1000));
			var expires = date.toUTCString();
			document.cookie = name + '=' + value + ';' + 'expires=' + expires + ';' + 'path=' + path + ';';
		}

		function cookieAction(e) {
			if(e == 'allow') {
				create_cookie('cookieconsent', 2, 30, "/"); // valid for 30 days
			} else {
				create_cookie('cookieconsent', 0, 1, "/"); // valid for 1 day
			}

			$('.cookie-banner').fadeOut();
		}
	</script>
	<div class="cookie-banner">
		<div class="cookie-left">
			We use cookies to ensure you get the best experience on our website.	
			<a href="<?=site_url('privacy')?>">Learn More</a>
		</div>
		<div class="cookie-right">
			<span class="reject" onclick="cookieAction('reject')">Reject</span>
			<span class="accept" onclick="cookieAction('allow');dataLayer.push({'event': 'virtualPageview','eventVirtualPageviewURL': '<?php echo current_url(); ?>','eventVirtualPageviewTitle': '<?php if(isset ($pagetitle)) { echo $pagetitle; } ?>','eventValue': '0',
	'eventNonInteraction': 'False'}); ">Allow Cookies</span>
		</div>
	</div>
	<?php } ?>

<!-- Javascript
================================================== -->
<!--[if IE]>
<script type="text/javascript" src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>inc/js/min/main-min.js"></script>

</body>
</html>