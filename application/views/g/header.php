<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<title><?php if(isset ($pagetitle)) { echo $pagetitle; } else {  echo 'Deepcove Opticians'; } ?></title>
<meta name="description" content="<?php if(isset($metadesc)) { echo $metadesc; }  ?>"/>
<?php if(isset($canonical)) { ?>
<link rel="canonical" href="<?php echo $canonical; ?>" />
<?php } ?>

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Open Graph
================================================== -->
<meta property="og:title" content="<?php echo $pagetitle?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo current_url()?>" />
<?php if(isset($ogimage)) {  ?>
<meta property="og:image" content="<?php echo site_url()?>uploads/slir/w1000-q90/<?php echo $ogimage?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo site_url()?>uploads/slir/w1000-q90/home-banner.jpg" />
<?php } ?>

<!-- Favicons
================================================== -->
<link rel="shortcut icon" href="<?php echo base_url()?>inc/img/Favicon_32.jpg">
<link rel="apple-touch-icon" href="<?php echo base_url()?>uploads/slir/w57-h57-c57x57/Favicon_152.jpg">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()?>uploads/slir/w72-h72-c72x72/Favicon_152.jpg">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>uploads/slir/w76-h76-c76x76/Favicon_152.jpg">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()?>uploads/slir/w114-h114-c114x114/Favicon_152.jpg">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url()?>uploads/slir/w120-h120-c120x120/Favicon_152.jpg">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url()?>uploads/slir/w144-h144-c144x144/Favicon_152.jpg">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url()?>uploads/slir/w152-h152-c152x152/Favicon_152.jpg">

<!-- CSS
================================================== -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link href="<?php echo base_url()?>inc/css/main.css?v=7" rel="stylesheet">

<!-- HEADER JS
================================================== -->
<script type="text/javascript" src="<?php echo base_url() ?>inc/js/modernizr-custom.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PS24TSC');</script>
<!-- End Google Tag Manager -->
</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PS24TSC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php if($this->uri->segment(1)) { 
	include 'global-nav.php'; 
} ?>