  <header>
	<div class="container white-mobile">
		<div class="row">
			<div class="navigation home-nav">
				<a href="<?php echo base_url() ?>">
					<div class="logo">
						<img src="<?php echo base_url()?>inc/img/white-logo.svg" alt="Deepcove EU" class="white-logo">
						<img src="<?php echo base_url()?>inc/img/logo.svg" alt="Deepcove EU" class="coloured-logo">						
					</div>
				</a>
				<div class="links-container">
					<ul class="vertical medium-horizontal menu">
					  <li><a href="<?php echo base_url()?>optiswiss">Optiswiss</a></li>
					  <li><a href="<?php echo base_url()?>deepcove">Deepcove Rx</a></li>
					  <li><a href="<?php echo base_url()?>order">Order</a></li>
					  <li><a href="<?php echo base_url()?>complete-spectacles">Complete Spectacles</a></li>
					  <li><a href="<?php echo base_url()?>labs">Labs</a></li>
					  <li><a href="<?php echo base_url()?>contact">Contact</a></li>
					</ul>
				</div>
				<div class="mobile-menu">
					<div id="nav-icon3" class="hamburger">
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row mobile-links-menu">
			<div class="menu">
			  <ul class="mobile-links">
				  <li><a href="<?php echo base_url()?>optiswiss">Optiswiss</a></li>
				  <li><a href="<?php echo base_url()?>deepcove">Deepcove Rx</a></li>
				  <li><a href="<?php echo base_url()?>order">Order</a></li>
				  <li><a href="<?php echo base_url()?>complete-spectacles">Complete Spectacles</a></li>
				  <li><a href="<?php echo base_url()?>labs">Labs</a></li>
				  <li><a href="<?php echo base_url()?>contact">Contact</a></li>
			  </ul>
			</div> 
		</div>
	</div>
</header>