<div class="container">

	<div class="relative">
		<div class="home-nav"><?php include 'g/home-nav.php'; ?></div>

		<!-- <div class="home-slider your-class">
				<div class="home-slide slick-slide first">
					<div class="home-slide-image"><img src="https://www.fillmurray.com/1200/470" alt=""></div>
					<div class="home-slide-text">
						<h2>Slide 1</h2>
						<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Harum, optio?</p>
					</div>
				</div>
				<div class="home-slide slick-slide second">
				<div class="home-slide-image"><img src="https://www.fillmurray.com/1200/610" alt=""></div>
					<div class="home-slide-text">
						<h2>Slide 2</h2>
						<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Harum, optio?</p>
					</div>
				</div>
				<div class="home-slide slick-slide third">
				<div class="home-slide-image"><img src="https://www.fillmurray.com/1200/620" alt=""></div>
					<div class="home-slide-text">
						<h2>Slide 3</h2>
						<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Harum, optio?</p>
					</div>
				</div>
				<div class="home-slide slick-slide fourth">
					<div class="home-slide-image"><img src="https://www.fillmurray.com/1200/630" alt=""></div>
					<div class="home-slide-text">
						<h2>Slide 4</h2>
						<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Harum, optio?</p>
					</div>
				</div>
				<div class="home-slide slick-slide fifth">
					<div class="home-slide-image"><img src="https://www.fillmurray.com/1200/640" alt=""></div>
					<div class="home-slide-text">
						<h2>Slide 5</h2>
						<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Harum, optio?</p>
					</div>
				</div>
		</div> -->

	</div>





	<div class="overlaying-image relative">
		<?php include 'g/home-nav.php'; ?>
		<div class="row text-center">
			<div class="column small-12 medium-8 medium-push-2 large-8 large-push-2 v-center main-content">
				<?php echo $row->text;?>
			</div>
		</div>
		<div class="row custom-align main-cta-container hidden-for-small">
			<div class="column medium-6 grey-box relative">
				<a href="/optiswiss" class="spanning">
					<img src="<?= base_url() ?>inc/img/optiswiss.svg" class="hv-center optiswiss" alt="Optiswiss">
					<p class="find-out uppercase">
						<a href="<?php echo base_url() ?>optiswiss">
							View lenses
							<img src="<?php echo base_url() ?>inc/img/black-next.svg" class="next">
						</a>
					</p>
				</a>
			</div>
			<div class="column medium-6 grey-box relative">
				<a href="/deepcove" class="spanning">
					<img src="<?= base_url() ?>inc/img/deepcove.svg" class="hv-center deepcove" alt="Deepcove EU">
					<p class="find-out uppercase">
						<a href="<?php echo base_url() ?>deepcove">
							View lenses
							<img src="<?php echo base_url() ?>inc/img/black-next.svg" class="next" />
						</a>
					</p>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="container home-body">
	<div class="row">
		<div class="mobile-cta-container">
			<div class="column medium-6 relative">
				<a href="/optiswiss" class="spanning">
					<div class="grey-box">
						<img src="<?php echo base_url() ?>inc/img/optiswiss.svg" class="hv-center optiswiss" alt="Optiswiss">
						<p class="find-out uppercase">
							<a href="<?php echo base_url() ?>optiswiss">
								View lenses
								<img src="<?php echo base_url() ?>inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</a>
			</div>
			<div class="column medium-6 relative">
				<div class="grey-box">
					<a href="/deepcove" class="spanning">
						<img src="<?= base_url() ?>inc/img/deepcove.svg" class="hv-center deepcove" alt="Deepcove EU">
						<p class="find-out uppercase">
							<a href="<?php echo base_url() ?>deepcove">
								View lenses
								<img src="<?php echo base_url() ?>inc/img/black-next.svg" class="next">
							</a>
						</p>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row text-center">
		<div class="column medium-10 medium-push-1 about-deepcove">



			<h2 class="uppercase"><?= $this->typography->format_characters($h->heading);?></h2>
			<div class="single-border"></div>
			<p><?= $this->typography->format_characters($h->body);?></p>
			<div class="button-container">
					<a class="primary" href="/about">
						<?php echo $this->typography->format_characters($h->buttontitle); ?>
						<img src="<?php echo base_url(); ?>inc/img/next.svg" class="next" />
					</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="column large-4 text-center panel-container">
			<a href="<?php echo base_url() ?>complete-spectacles" class="spanning">
				<div class="panel relative">
					<div class="panel-content v-center">
						<div class="panel-image-container">
							<img src="<?php echo base_url() ?>/inc/img/specs.svg" alt="Spectacles" class="panel-image specs hv-center" />
						</div>
						<h4><?= $this->typography->format_characters($h->boxonetitle); ?></h4>
						<div class="small-single-border"></div>
						<p><?= $this->typography->format_characters($h->boxonebody); ?><p>
						<p class="find-out">
							<a href="<?php echo base_url() ?>complete-spectacles">
								<?= $this->typography->format_characters($h->buttontitle); ?>
								<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</div>
			</a>
		</div>
		<div class="column large-4 text-center panel-container">
			<a href="<?php echo base_url() ?>labs" class="spanning">
				<div class="panel relative">
					<div class="panel-content v-center">
						<div class="panel-image-container">
							<img src="<?php echo base_url() ?>/inc/img/microscope.svg" alt="Microscope" class="panel-image labs hv-center"  />
						</div>
						<h4><?php echo $this->typography->format_characters($h->boxtwotitle); ?></h4>
						<div class="small-single-border"></div>
						<p><?php echo $this->typography->format_characters($h->boxtwobody); ?></p>
						<p class="find-out">
							<a href="<?php echo base_url() ?>labs">
								<?php echo $this->typography->format_characters($h->buttontitle); ?>
								<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</div>
			</a>
		</div>
		<div class="column large-4 text-center panel-container">
			<a href="<?php echo base_url() ?>marketing" class="spanning">
				<div class="panel relative">
					<div class="panel-content v-center">
						<div class="panel-image-container">
							<img src="<?php echo base_url() ?>/inc/img/messages.svg" alt="Messages" class="panel-image messages hv-center"  />
						</div>
						<h4><?php echo $this->typography->format_characters($h->boxthreetitle); ?></h4>
						<div class="small-single-border"></div>
						<p><?php echo $this->typography->format_characters($h->boxthreebody); ?><p>
						<p class="find-out">
							<a href="<?php echo base_url() ?>marketing">
								<?php echo $this->typography->format_characters($h->buttontitle); ?>
								<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>