<div class="container contact">
	<div class="row">
		<div class="columns small-12 large-8 large-push-2 text-center">
			<div class="main-content">
				<h1 class="uppercase">Contact Us</h1>
				<p>We have a specialist team of qualified opticians who are on hand to answer your questions.</p>
			</div>
		</div>
	</div>
	<div class="row bottom-row">
		<div class="columns small-12 medium-6 large-4 large-push-2 uppercase">
			<div class="text-container">
				<p>
					Unit 9 Rockhaven Business Centre <br>
					Longhedge <br>
					Salisbury <br >
					SP4 6RT <br>
					Wiltshire <br>
					United Kingdom
				</p>
			</div>
			<div class="icon-container">
				<div><span class="headphones"></span><span class="telephone"><a href="tel:+441985800425">+44 (0) 1985 800 425</a></span></div>
				<div><span class="email"></span><a href="mailto:info@deepcove.co.uk">info@deepcove.co.uk</a></div>
			</div>
		</div>
		<div class="columns small-12 medium-6 large-4 large-pull-2 uppercase">
			<div class="people-container">
				<div class="person">
					<p class="name">
						Customer Service Team
					</p>
					<p class="tel">
						Direct: <a href="tel:+441985250121">+44 (0) 1985 250 121</a>
					</p>
					<p class="email">
						<a href="mailto:customer.service@deepcove.co.uk">customer.service@deepcove.co.uk</a>
					</p>
				</div>
				<div class="person">
					<p class="name">
						Roland Allen
					</p>
					<p class="tel">
						<a href="tel:+447969015944">+44 (0) 7969 015 944</a>
					</p>
					<p class="email">
						<a href="mailto:roland.allen@@deepcove.co.uk">roland.allen@deepcove.co.uk</a>
					</p>
				</div>
				<div class="person">
					<p class="name">
						Adrian Huluban
					</p>
					<p class="tel">
						<a href="tel:+447807794806">+44 (0) 7808 794 806</a>
					</p>
					<p class="email">
						<a href="mailto:adrian.huluban@deepcove.co.uk">adrian.huluban@deepcove.co.uk</a>
					</p>
				</div>
				<div class="person">
					<p class="name">
						Jack Carter
					</p>
					<p class="tel">
						<a href="tel:+447790991819">+44 (0) 7790 991 819</a>
					</p>
					<p class="email">
						<a href="mailto:jack.carter@deepcove.co.uk">jack.carter@deepcove.co.uk</a>
					</p>
				</div>				
			</div>
		</div>
	</div>
</div>
<div class="container contact-form light-grey-background" id="formanchor">
	<div class="row text-center contact-us">
		<h2>Get in touch</h2>
	</div>
	<?php echo form_open('contact/submitform', 'id="contactform" autocomplete="off"')?>
		<div class="row uppercase">
			<div class="columns small-12 medium-6">
				<label> Name *
					<input id="name" type="text" name="name" class="required" value="<?php echo $this->session->flashdata('name'); ?>" required="" />
				</label>
			</div>
			<div class="columns small-12 medium-6">
				<label> Email *
					<input id="email" type="email" name="email" class="required" value="<?php echo $this->session->flashdata('email'); ?>" required="" />
				</label>
			</div>
		</div>
		<div class="row uppercase">
			<div class="columns small-12 medium-6">
				<label> Telephone *
					<input id="telephonenum" type="tel" name="telephonenum" class="required" value="<?php echo $this->session->flashdata('telephonenum'); ?>" required="" />
				</label>
			</div>
			<div class="columns small-12 medium-6">
				<label> Company *
					<input id="company" type="text" name="company" class="required" value="<?php echo $this->session->flashdata('company'); ?>" required="" />
				</label>
			</div>
		</div>
		<div class="row uppercase">
			<div class="columns small-12">
				<label>Message</label>
				<textarea id="comment" rows="10" type="text" name="comment" required=""><?php echo $this->session->flashdata('comment'); ?></textarea>
			</div>
		</div>
		<div class="row">
			<div class="columns small-12">
				<p>We will never share or sell your personal data and we promise to keep your details safe and secure.</p>
			</div>
		</div>
		<?php if($this->session->flashdata('error')) { ?>
			<div class="row uppercase">
				<div class="columns small-12">
					<div class="errordiv">Please tick the box below to confirm that you are not a robot</div>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<div class="columns small-12 medium-6">
				<div class="captcha-container">
					<?php echo $this->recaptcha->render(); ?>
				</div>
			</div>

			<div class="columns small-12 medium-6">
				<div class="button-container">
					<?php form_hidden('alternate'); //honeypot ?>
					<input class="button primary float-right" type="submit" value="Submit" />
					<img src="<?php echo base_url() ?>/inc/img/next.svg" alt="Deepcove EU" class="next">
				</div>
			</div>
		</div>


	</form>
</div>