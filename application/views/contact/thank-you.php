<div class="container thank-you">
	<div class="row">
		<div class="column small-12 text-center">
			<p>Thank you for getting in touch</p>
			<p>One of our team members will call you back as soon as possible, normally within the next 24 hours. <br>
			Deepcove Optical Team </p>
		</div>
	</div>
</div>