<div class="top-strip">
	<div class="row">
		<div class="columns small-12 text-center">
			<p class="text-center uppercase"><a href="<?php base_url() ?>register">Create account</a> to place an order</p>
		</div>
	</div>
</div>
<div class="container brands relative">
	<div class="row main-container">
		<div class="column small-12 medium-8 medium-push-2 text-center relative">
			<div class="main-content">
				<h1 class="uppercase">ORDER LENSES</h1>
				<p>Account holders only.</p>
			</div>
		</div>
	</div>
	<div class="row absolute-row">
		<div class="column small-12 medium-6 text-center">
			<div class="grey-box relative">
				<a href="https://distributor.optiswiss.com/" target="_blank" class="spanning">
					<img src="<?php echo base_url(); ?>inc/img/optiswiss.svg" alt="Optiswiss" class="hv-center">
					<div class="text-container text-center">
						<p class="find-out">
							<a href="https://distributor.optiswiss.com/" target="_blank" class="uppercase">
								Order lenses					
								<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</a>
			</div>
		</div>
		<div class="column small-12 medium-6 text-center">
			<div class="grey-box relative">
				<a href="/deepcove/iframe" class="spanning">
				<img src="<?php echo base_url(); ?>inc/img/deepcove.svg" alt="Deepcove" class="hv-center">
					<div class="text-container text-center">
						<p class="find-out">
							<a href="<?php echo base_url() ?>deepcove/iframe" class="uppercase">
								Order lenses
								<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="container show-for-small mobile-cta-container">
	<div class="row">
		<div class="column small-12 medium-6 text-center">
			<div class="grey-box relative">
				<a href="https://distributor.optiswiss.com/" target="_blank" class="spanning">
					<img src="<?php echo base_url(); ?>inc/img/optiswiss.svg" alt="Optiswiss" class="hv-center optiswiss">
					<div class="text-container text-center">
						<p class="find-out">
							<a href="https://distributor.optiswiss.com/" target="_blank" class="uppercase">
								Order lenses					
								<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</a>
			</div>
		</div>
		<div class="column small-12 medium-6 text-center">
			<div class="grey-box relative">
				<a href="/deepcove/iframe" class="spanning">
				<img src="<?php echo base_url(); ?>inc/img/deepcove.svg" alt="Deepcove" class="hv-center deepcove">
					<div class="text-container text-center">
						<p class="find-out">
							<a href="<?php echo base_url() ?>deepcove/iframe" class="uppercase">
								Order lenses
								<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>