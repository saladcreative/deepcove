<div class="register">
	<div class="container light-grey-body">
		<div class="row">
			<div class="columns small-12">
				<div class="content-container">
					<div class="small-single-border"></div>
					<h1 class="uppercase"><?php echo $this->typography->format_characters($r->title);?></h1>
					<p><?php echo $this->typography->format_characters($r->body);?></p>
				</div>
			</div>
		</div>
		<?php echo form_open('register/createAccount', 'id="contactform" autocomplete="off"')?>
			<div class="row uppercase">
				<div class="columns small-12 medium-6">
					<label> Company *
						<input id="companyname" type="text" name="companyname" class="required" value="" required="" />
					</label>
					<label>Country *
						<input id="country" type="text" name="country" class="required" value="" required="" />
					</label>
					<label>Title *
						<?php $title = array(
								'Mr' => 'Mr',
								'Ms' => 'Ms',
								'Miss' => 'Miss',
								'Mrs' => 'Mrs',
							); ?>
							<?php echo form_dropdown('title', $title, '', 'class="required ss" id="title"'); ?>
					</label>
					<label> First Name *
						<input id="firstname" type="text" name="firstname" class="required" value="" required="" />
					</label>
					<label> Surname *
						<input id="lastname" type="text" name="lastname" class="required" value="" required="" />
					</label>
					<label> Street 
						<input id="street" type="text" name="street" value="" />
					</label>
					<label> Postcode * 
						<input id="postcode" type="text" name="postcode" class="required" value="" required="" />
					</label>
					<label> City 
						<input id="city" type="text" name="city" value="" />
					</label>
				</div>
				<div class="columns small-12 medium-6">
					<label> Telephone *
						<input id="telephone" type="tel" name="telephone" class="required" value="" required="" />
					</label>
					<label> Email Main Contact *
						<input id="emailmain" type="email" name="emailmain" class="required" value="" required="" />
					</label>
					<label> Email Accounting Department *
						<input id="emailaccounting" type="email" name="emailaccounting" class="required"  required="" value="" />
					</label>
					<label> VAT 
						<input id="vat" type="text" name="vat" class="required" value="" />
					</label>
					<label> Comment
						<textarea id="comment" rows="16" type="text" name="comment"></textarea>
					</label>
				</div>
			</div>
			<div class="row tsandcs uppercase">
				<div class="columns medium-6 float-right">
					<p class="uppercase select-brand">Please select the brand you would like to order:</p>
					<div class="row checkbox-container">
						<div class="small-12">
							<input id="optiswiss" type="checkbox" name="optiswiss" value="1"><label for="optiswiss">Optiswiss</label>
						</div>
						<div class="small-12">
							<input id="deepcove" type="checkbox" name="deepcove" value="1"><label for="deepcove">Deepcove</label>
						</div>
						<div class="small-12">
							<input id="tsAndcs" type="checkbox" required><label id="termslabel" for="tsAndcs">I agree with the <a href="/terms" target="_blank" class="underline">General terms and conditions</a></label>
						</div>
					</div>
					<div class="float-right">
						<?php form_hidden('alternate'); //honeypot ?>
						<input type="submit" class="button primary" value="Register" />
						<img src="<?php echo base_url(); ?>/inc/img/next.svg" class="next" />
					</div>
				</div>
			</div>
		</form>
	</div>
</div>