
<div class="container brands relative">
	<div class="row main-container">
		<div class="column small-12 medium-8 medium-push-2 text-center relative">
			<div class="main-content">
				<h1 class="uppercase">404</h1>
				<p>Page not found.</p>
			</div>
		</div>
	</div>
	<div class="row absolute-row">
		<div class="column small-12 medium-6 text-center">
			<div class="grey-box relative">
				<a href="https://www.optiswiss.com/en/services/online-shop-apps/online-shop.php" target="_blank" class="spanning">
					<img src="<?php echo base_url(); ?>inc/img/optiswiss.svg" alt="Optiswiss" class="hv-center">
					<div class="text-container text-center">
						<p class="find-out">
							<a href="https://www.optiswiss.com/en/services/online-shop-apps/online-shop.php" target="_blank" class="uppercase">
								Order lenses					
								<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</a>
			</div>
		</div>
		<div class="column small-12 medium-6 text-center">
			<div class="grey-box relative">
				<a href="/deepcove/iframe" class="spanning">
				<img src="<?php echo base_url(); ?>inc/img/deepcove.svg" alt="Deepcove" class="hv-center">
					<div class="text-container text-center">
						<p class="find-out">
							<a href="<?php echo base_url() ?>deepcove/iframe" class="uppercase">
								Order lenses
								<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="container show-for-small mobile-cta-container">
	<div class="row">
		<div class="column small-12 medium-6 text-center">
			<div class="grey-box relative">
				<a href="https://www.optiswiss.com/en/services/online-shop-apps/online-shop.php" target="_blank" class="spanning">
					<img src="<?php echo base_url(); ?>inc/img/optiswiss.svg" alt="Optiswiss" class="hv-center">
					<div class="text-container text-center">
						<p class="find-out">
							<a href="" class="uppercase">
								Order lenses					
								<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</a>
			</div>
		</div>
		<div class="column small-12 medium-6 text-center">
			<div class="grey-box relative">
				<a href="/" class="spanning">
				<img src="<?php echo base_url(); ?>inc/img/deepcove.svg" alt="Deepcove" class="hv-center">
					<div class="text-container text-center">
						<p class="find-out">
							<a href="<?php echo base_url() ?>deepcove/iframe" class="uppercase">
								Order lenses
								<img src="<?php echo base_url() ?>/inc/img/black-next.svg" class="next">
							</a>
						</p>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>