<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

// Home
$route['default_controller'] = "home";
$route['404_override'] = 'custom404';

$route['optiswiss/lenses'] = "optiswiss/landing_page";
$route['optiswiss/lenses/(:any)'] = "optiswiss/view_article/$1";
$route['optiswiss/coatings'] = "optiswiss/landing_page";
$route['optiswiss/coatings/(:any)'] = "optiswiss/view_article/$1";
$route['optiswiss/special-products'] = "optiswiss/landing_page";
$route['optiswiss/special-products/(:any)'] = "optiswiss/view_article/$1";

$route['deepcove/lenses'] = "deepcove/landing_page";
$route['deepcove/lenses/(:any)'] = "deepcove/view_article/$1";
$route['deepcove/treatments'] = "deepcove/landing_page";
$route['deepcove/treatments/(:any)'] = "deepcove/view_article/$1";
$route['deepcove/extraservices'] = "deepcove/landing_page";
$route['deepcove/extraservices/(:any)'] = "deepcove/view_article/$1";
$route['deepcove/iframe'] = "home/iframe";

// Text pages
$route['marketing'] = "home/template_page";
$route['complete-spectacles'] = "home/template_page";
$route['labs'] = "home/template_page";
$route['privacy'] = "home/template_page";
$route['about'] = "home/template_page";
$route['terms'] = "home/template_page";

$route['order'] = "brands/index";
$route['contact'] = "contact/index";
$route['thank-you'] = "contact/thankyou";
$route['account/thank-you'] = "register/thankyou";

$route['register'] = "register/index";


/* End of file routes.php */
/* Location: ./application/config/routes.php */